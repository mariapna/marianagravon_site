<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'projetos_marianagravon_site' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ':m)le7 ;W{{fT4UQeS2hS8fbp+$&c/xyLnK3weG:3wqUl{,da~?aeMFMU +H&2~F' );
define( 'SECURE_AUTH_KEY',  'y AVw%9}h@-ukmb>o26wN7&lXr%#UK$W@m}$Ia,x<dm/]|=>zgJwy>`2I#QeH|Z?' );
define( 'LOGGED_IN_KEY',    'jrvM$IIk?OYyh^{>dReNqlc=o_F1]p@`6K5YuEE^>VQlkY)2S:>g7*8wOGrif}`K' );
define( 'NONCE_KEY',        '5;-0}zQvi`G!Mz}p#6nosBB:EJ](48l.>lEA)B~XrM.YLV5/1_#zFqNA+<rI8v`y' );
define( 'AUTH_SALT',        '](dTkx|#MpnN)L{6)j2%B/$%+dbs6u:ZlY2cuX]5#~fN-G ,tKKZk9zYU+,}1CoN' );
define( 'SECURE_AUTH_SALT', '<zH*@9v`6D9<EPP:D}+nw@,Y,IoFd.*9c*:-s?xeaX =O}tVZP~:#Dcp2<_;_TVa' );
define( 'LOGGED_IN_SALT',   'jEOX7y]S@*-k>N0niJzGkCT5j[D8p%Tdi4OO<Gc3:`&F42|=Ap3s5@uDKi.T1m4k' );
define( 'NONCE_SALT',       '9r76J@vF1=a4-)ZZSYI-gBq+=g@Y7f2Yd(Yy!uux?O ^Bq5n$K4b9@9NxO& `w?8' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'mg_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
