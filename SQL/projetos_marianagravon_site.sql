-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 16-Mar-2019 às 21:11
-- Versão do servidor: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projetos_marianagravon_site`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `mg_commentmeta`
--

DROP TABLE IF EXISTS `mg_commentmeta`;
CREATE TABLE IF NOT EXISTS `mg_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `mg_comments`
--

DROP TABLE IF EXISTS `mg_comments`;
CREATE TABLE IF NOT EXISTS `mg_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `mg_comments`
--

INSERT INTO `mg_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Um comentarista do WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-03-14 16:32:23', '2019-03-14 19:32:23', 'Olá, isso é um comentário.\nPara começar a moderar, editar e excluir comentários, visite a tela de Comentários no painel.\nAvatares de comentaristas vêm a partir do <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `mg_links`
--

DROP TABLE IF EXISTS `mg_links`;
CREATE TABLE IF NOT EXISTS `mg_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `mg_options`
--

DROP TABLE IF EXISTS `mg_options`;
CREATE TABLE IF NOT EXISTS `mg_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=MyISAM AUTO_INCREMENT=266 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `mg_options`
--

INSERT INTO `mg_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/projetos/marianagravon_site', 'yes'),
(2, 'home', 'http://localhost/projetos/marianagravon_site', 'yes'),
(3, 'blogname', 'Mariana Gravon Arquitetura', 'yes'),
(4, 'blogdescription', 'Só mais um site WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'devhcdesenvolvimentos@gmail.com', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:161:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:12:\"portfolio/?$\";s:29:\"index.php?post_type=portfolio\";s:42:\"portfolio/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?post_type=portfolio&feed=$matches[1]\";s:37:\"portfolio/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?post_type=portfolio&feed=$matches[1]\";s:29:\"portfolio/page/([0-9]{1,})/?$\";s:47:\"index.php?post_type=portfolio&paged=$matches[1]\";s:11:\"servicos/?$\";s:27:\"index.php?post_type=servico\";s:41:\"servicos/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=servico&feed=$matches[1]\";s:36:\"servicos/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=servico&feed=$matches[1]\";s:28:\"servicos/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=servico&paged=$matches[1]\";s:12:\"destaques/?$\";s:28:\"index.php?post_type=destaque\";s:42:\"destaques/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:37:\"destaques/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:29:\"destaques/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=destaque&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:37:\"portfolio/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:47:\"portfolio/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:67:\"portfolio/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"portfolio/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"portfolio/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:43:\"portfolio/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:26:\"portfolio/([^/]+)/embed/?$\";s:42:\"index.php?portfolio=$matches[1]&embed=true\";s:30:\"portfolio/([^/]+)/trackback/?$\";s:36:\"index.php?portfolio=$matches[1]&tb=1\";s:50:\"portfolio/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?portfolio=$matches[1]&feed=$matches[2]\";s:45:\"portfolio/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?portfolio=$matches[1]&feed=$matches[2]\";s:38:\"portfolio/([^/]+)/page/?([0-9]{1,})/?$\";s:49:\"index.php?portfolio=$matches[1]&paged=$matches[2]\";s:45:\"portfolio/([^/]+)/comment-page-([0-9]{1,})/?$\";s:49:\"index.php?portfolio=$matches[1]&cpage=$matches[2]\";s:34:\"portfolio/([^/]+)(?:/([0-9]+))?/?$\";s:48:\"index.php?portfolio=$matches[1]&page=$matches[2]\";s:26:\"portfolio/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:36:\"portfolio/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:56:\"portfolio/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"portfolio/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"portfolio/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:32:\"portfolio/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:36:\"servicos/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"servicos/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"servicos/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"servicos/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"servicos/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"servicos/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"servicos/([^/]+)/embed/?$\";s:40:\"index.php?servico=$matches[1]&embed=true\";s:29:\"servicos/([^/]+)/trackback/?$\";s:34:\"index.php?servico=$matches[1]&tb=1\";s:49:\"servicos/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?servico=$matches[1]&feed=$matches[2]\";s:44:\"servicos/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?servico=$matches[1]&feed=$matches[2]\";s:37:\"servicos/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?servico=$matches[1]&paged=$matches[2]\";s:44:\"servicos/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?servico=$matches[1]&cpage=$matches[2]\";s:33:\"servicos/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?servico=$matches[1]&page=$matches[2]\";s:25:\"servicos/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"servicos/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"servicos/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"servicos/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"servicos/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"servicos/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:37:\"destaques/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:47:\"destaques/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:67:\"destaques/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"destaques/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"destaques/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:43:\"destaques/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:26:\"destaques/([^/]+)/embed/?$\";s:41:\"index.php?destaque=$matches[1]&embed=true\";s:30:\"destaques/([^/]+)/trackback/?$\";s:35:\"index.php?destaque=$matches[1]&tb=1\";s:50:\"destaques/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:45:\"destaques/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:38:\"destaques/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&paged=$matches[2]\";s:45:\"destaques/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&cpage=$matches[2]\";s:34:\"destaques/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?destaque=$matches[1]&page=$matches[2]\";s:26:\"destaques/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:36:\"destaques/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:56:\"destaques/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"destaques/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"destaques/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:32:\"destaques/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:60:\"categoria-portfolio/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:57:\"index.php?categoriaPortfolio=$matches[1]&feed=$matches[2]\";s:55:\"categoria-portfolio/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:57:\"index.php?categoriaPortfolio=$matches[1]&feed=$matches[2]\";s:36:\"categoria-portfolio/([^/]+)/embed/?$\";s:51:\"index.php?categoriaPortfolio=$matches[1]&embed=true\";s:48:\"categoria-portfolio/([^/]+)/page/?([0-9]{1,})/?$\";s:58:\"index.php?categoriaPortfolio=$matches[1]&paged=$matches[2]\";s:30:\"categoria-portfolio/([^/]+)/?$\";s:40:\"index.php?categoriaPortfolio=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=6&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:5:{i:0;s:35:\"redux-framework/redux-framework.php\";i:1;s:27:\"base-modelo/base-modelo.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:21:\"meta-box/meta-box.php\";i:4;s:37:\"post-types-order/post-types-order.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'mariana_gravon', 'yes'),
(41, 'stylesheet', 'mariana_gravon', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '6', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'initial_db_version', '44719', 'yes'),
(94, 'mg_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', 'pt_BR', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'cron', 'a:5:{i:1552771944;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1552807944;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1552851163;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1552851165;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(113, 'theme_mods_twentynineteen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1552592052;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(257, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.1.1.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.1.1.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.1.1\";s:7:\"version\";s:5:\"5.1.1\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1552764752;s:15:\"version_checked\";s:5:\"5.1.1\";s:12:\"translations\";a:0:{}}', 'no'),
(180, 'redux_version_upgraded_from', '3.6.15', 'yes'),
(255, '_site_transient_timeout_theme_roots', '1552766549', 'no'),
(256, '_site_transient_theme_roots', 'a:1:{s:14:\"mariana_gravon\";s:7:\"/themes\";}', 'no'),
(258, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1552764753;s:7:\"checked\";a:1:{s:14:\"mariana_gravon\";s:5:\"1.0.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(121, '_site_transient_timeout_browser_75b3341da9e7208fc03d0909f69991aa', '1553196764', 'no'),
(122, '_site_transient_browser_75b3341da9e7208fc03d0909f69991aa', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"72.0.3626.121\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(123, '_site_transient_timeout_php_check_464f4068caea2f8f3edcc5ae59429c65', '1553196765', 'no'),
(124, '_site_transient_php_check_464f4068caea2f8f3edcc5ae59429c65', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:5:\"5.2.4\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(126, 'can_compress_scripts', '1', 'no'),
(244, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(245, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.1.1\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";d:1552716563;s:7:\"version\";s:5:\"5.1.1\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(197, 'CPT_configured', 'TRUE', 'yes'),
(203, 'categoriaPortfolio_children', 'a:0:{}', 'yes'),
(139, 'current_theme', 'marianagravon', 'yes'),
(140, 'theme_mods_mariana_gravon', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(141, 'theme_switched', '', 'yes'),
(146, 'category_children', 'a:0:{}', 'yes'),
(149, '_transient_timeout_plugin_slugs', '1552813765', 'no'),
(150, '_transient_plugin_slugs', 'a:6:{i:0;s:27:\"base-modelo/base-modelo.php\";i:1;s:36:\"contact-form-7/wp-contact-form-7.php\";i:2;s:21:\"meta-box/meta-box.php\";i:3;s:37:\"post-types-order/post-types-order.php\";i:4;s:35:\"redux-framework/redux-framework.php\";i:5;s:24:\"wordpress-seo/wp-seo.php\";}', 'no'),
(151, 'recently_activated', 'a:0:{}', 'yes'),
(220, '_site_transient_timeout_php_check_d005457bdf39fe8b07a9eaff24a9225e', '1553324012', 'no'),
(221, '_site_transient_php_check_d005457bdf39fe8b07a9eaff24a9225e', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:5:\"5.2.4\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:0;s:13:\"is_acceptable\";b:1;}', 'no'),
(222, '_site_transient_timeout_community-events-d41d8cd98f00b204e9800998ecf8427e', '1552804911', 'no'),
(223, '_site_transient_community-events-d41d8cd98f00b204e9800998ecf8427e', 'a:2:{s:8:\"location\";a:1:{s:2:\"ip\";b:0;}s:6:\"events\";a:1:{i:0;a:7:{s:4:\"type\";s:6:\"meetup\";s:5:\"title\";s:30:\"44º Meetup WordPress Curitiba\";s:3:\"url\";s:51:\"https://www.meetup.com/wpcuritiba/events/259187882/\";s:6:\"meetup\";s:29:\"Comunidade WordPress Curitiba\";s:10:\"meetup_url\";s:34:\"https://www.meetup.com/wpcuritiba/\";s:4:\"date\";s:19:\"2019-03-27 19:00:00\";s:8:\"location\";a:4:{s:8:\"location\";s:16:\"Curitiba, Brazil\";s:7:\"country\";s:2:\"br\";s:8:\"latitude\";d:-25.443770000000001;s:9:\"longitude\";d:-49.28819;}}}}', 'no'),
(263, '_transient_timeout_select2-js_script_cdn_is_up', '1552851690', 'no'),
(264, '_transient_select2-js_script_cdn_is_up', '1', 'no'),
(196, 'cpto_options', 'a:7:{s:23:\"show_reorder_interfaces\";a:5:{s:4:\"post\";s:4:\"show\";s:10:\"attachment\";s:4:\"show\";s:8:\"wp_block\";s:4:\"show\";s:9:\"portfolio\";s:4:\"show\";s:7:\"servico\";s:4:\"show\";}s:8:\"autosort\";i:1;s:9:\"adminsort\";i:1;s:18:\"use_query_ASC_DESC\";s:0:\"\";s:17:\"archive_drag_drop\";i:1;s:10:\"capability\";s:14:\"manage_options\";s:21:\"navigation_sort_apply\";i:1;}', 'yes'),
(182, 'configuracao', 'a:18:{s:8:\"last_tab\";s:1:\"1\";s:8:\"opt_logo\";a:9:{s:3:\"url\";s:80:\"http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/logo.png\";s:2:\"id\";s:2:\"20\";s:6:\"height\";s:3:\"109\";s:5:\"width\";s:3:\"387\";s:9:\"thumbnail\";s:88:\"http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/logo-150x109.png\";s:5:\"title\";s:4:\"logo\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:18:\"opt_texto_destaque\";s:84:\"simples traços são<br><strong>a projeção de grandes</strong> <span>sonhos</span>\";s:8:\"opt_foto\";a:9:{s:3:\"url\";s:86:\"http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/foto-sobre.png\";s:2:\"id\";s:2:\"30\";s:6:\"height\";s:3:\"662\";s:5:\"width\";s:3:\"623\";s:9:\"thumbnail\";s:94:\"http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/foto-sobre-150x150.png\";s:5:\"title\";s:10:\"foto-sobre\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:10:\"opt_titulo\";s:27:\"Olá! Eu sou Mariana Gravon\";s:8:\"opt_desc\";s:662:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque diam volutpat commodo sed egestas egestas fringilla phasellus. Cursus in hac habitasse platea dictumst. Est ante in nibh mauris cursus. Ac tortor dignissim convallis aenean et. Pharetra magna ac placerat vestibulum. Purus semper eget duis at tellus at urna. Malesuada bibendum arcu vitae elementum. Imperdiet sed euismod nisi porta. Tellus pellentesque eu tincidunt tortor.\r\n\r\nAc auctor augue mauris augue neque gravida in fermentum et. Nunc lobortis mattis aliquam faucibus purus. Tincidunt praesent semper feugiat nibh sed.\";s:8:\"opt_nome\";s:14:\"Mariana Gravon\";s:12:\"opt_endereco\";s:65:\"Rua Lorem Ipsum, 123 - Sala 01\r\n\r\nGuarapuava | PR\r\n\r\n12 3456 7890\";s:18:\"opt_icone_intagram\";a:9:{s:3:\"url\";s:81:\"http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/photo.png\";s:2:\"id\";s:2:\"39\";s:6:\"height\";s:2:\"56\";s:5:\"width\";s:2:\"55\";s:9:\"thumbnail\";s:81:\"http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/photo.png\";s:5:\"title\";s:5:\"photo\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:20:\"opt_titulo_instagram\";s:38:\"@marianagravonarquitetura no Instagram\";s:13:\"opt_copyright\";s:31:\"Copyright 2019 - Mariana Gravon\";s:9:\"opt_phone\";s:15:\"(41) 99946-5775\";s:9:\"opt_email\";s:28:\"contato@marianagavron.com.br\";s:12:\"opt_facebook\";s:39:\"https://www.facebook.com/mariana.gravon\";s:13:\"opt_instagram\";s:51:\"https://www.instagram.com/marianagravonarquitetura/\";s:13:\"opt_pinterest\";s:1:\"#\";s:12:\"opt_linkedin\";s:1:\"#\";s:19:\"opt-customizer-only\";s:1:\"2\";}', 'yes'),
(183, 'configuracao-transients', 'a:2:{s:14:\"changed_values\";a:1:{s:20:\"opt_titulo_instagram\";s:27:\"@marianagravon no Instagram\";}s:9:\"last_save\";i:1552766457;}', 'yes'),
(218, '_site_transient_timeout_browser_591a43aafb2c3e203f6b925e7cd15e92', '1553324011', 'no'),
(219, '_site_transient_browser_591a43aafb2c3e203f6b925e7cd15e92', 'a:10:{s:4:\"name\";s:7:\"Firefox\";s:7:\"version\";s:4:\"65.0\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:24:\"https://www.firefox.com/\";s:7:\"img_src\";s:44:\"http://s.w.org/images/browsers/firefox.png?1\";s:11:\"img_src_ssl\";s:45:\"https://s.w.org/images/browsers/firefox.png?1\";s:15:\"current_version\";s:2:\"56\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(261, '_transient_timeout_select2-css_style_cdn_is_up', '1552851690', 'no'),
(262, '_transient_select2-css_style_cdn_is_up', '1', 'no'),
(259, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1552764754;s:7:\"checked\";a:6:{s:27:\"base-modelo/base-modelo.php\";s:3:\"0.1\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.1.1\";s:21:\"meta-box/meta-box.php\";s:6:\"4.16.3\";s:37:\"post-types-order/post-types-order.php\";s:7:\"1.9.3.9\";s:35:\"redux-framework/redux-framework.php\";s:6:\"3.6.15\";s:24:\"wordpress-seo/wp-seo.php\";s:4:\"10.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:3:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"contact-form-7\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:3:\"5.0\";s:7:\"updated\";s:19:\"2018-02-03 16:12:23\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/translation/plugin/contact-form-7/5.0/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:8:\"meta-box\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:6:\"4.16.3\";s:7:\"updated\";s:19:\"2018-12-05 02:06:02\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/translation/plugin/meta-box/4.16.3/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:2;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:13:\"wordpress-seo\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:4:\"10.0\";s:7:\"updated\";s:19:\"2019-03-06 19:03:30\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/translation/plugin/wordpress-seo/10.0/pt_BR.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:5:{s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.1.1\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.1.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:21:\"meta-box/meta-box.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:22:\"w.org/plugins/meta-box\";s:4:\"slug\";s:8:\"meta-box\";s:6:\"plugin\";s:21:\"meta-box/meta-box.php\";s:11:\"new_version\";s:6:\"4.16.3\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/meta-box/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/meta-box.4.16.3.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:63:\"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1929588\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"post-types-order/post-types-order.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:30:\"w.org/plugins/post-types-order\";s:4:\"slug\";s:16:\"post-types-order\";s:6:\"plugin\";s:37:\"post-types-order/post-types-order.php\";s:11:\"new_version\";s:7:\"1.9.3.9\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/post-types-order/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/post-types-order.1.9.3.9.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/post-types-order/assets/icon-128x128.png?rev=1226428\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/post-types-order/assets/banner-1544x500.png?rev=1675574\";s:2:\"1x\";s:71:\"https://ps.w.org/post-types-order/assets/banner-772x250.png?rev=1429949\";}s:11:\"banners_rtl\";a:0:{}}s:35:\"redux-framework/redux-framework.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/redux-framework\";s:4:\"slug\";s:15:\"redux-framework\";s:6:\"plugin\";s:35:\"redux-framework/redux-framework.php\";s:11:\"new_version\";s:6:\"3.6.15\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/redux-framework/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/redux-framework.3.6.15.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/redux-framework/assets/icon-256x256.png?rev=995554\";s:2:\"1x\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";s:3:\"svg\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=793165\";}s:11:\"banners_rtl\";a:0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:4:\"10.0\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wordpress-seo.10.0.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=1834347\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}}}}', 'no');

-- --------------------------------------------------------

--
-- Estrutura da tabela `mg_postmeta`
--

DROP TABLE IF EXISTS `mg_postmeta`;
CREATE TABLE IF NOT EXISTS `mg_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=293 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `mg_postmeta`
--

INSERT INTO `mg_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 2, '_wp_trash_meta_status', 'publish'),
(4, 2, '_wp_trash_meta_time', '1552592097'),
(5, 2, '_wp_desired_post_slug', 'pagina-exemplo'),
(6, 6, '_edit_lock', '1552593686:1'),
(7, 6, '_wp_page_template', 'pages/inicial.php'),
(8, 8, '_edit_lock', '1552651196:1'),
(9, 9, '_edit_lock', '1552651224:1'),
(10, 9, '_wp_page_template', 'pages/sobre.php'),
(11, 11, '_edit_lock', '1552651484:1'),
(12, 12, '_edit_lock', '1552651501:1'),
(13, 12, '_wp_page_template', 'pages/servicos.php'),
(14, 14, '_edit_lock', '1552651514:1'),
(15, 14, '_wp_page_template', 'pages/contato.php'),
(16, 16, '_edit_lock', '1552653694:1'),
(17, 16, '_wp_page_template', 'pages/portfolio.php'),
(18, 18, '_edit_lock', '1552651543:1'),
(19, 20, '_wp_attached_file', '2019/03/logo.png'),
(20, 20, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:387;s:6:\"height\";i:109;s:4:\"file\";s:16:\"2019/03/logo.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-150x109.png\";s:5:\"width\";i:150;s:6:\"height\";i:109;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"logo-300x84.png\";s:5:\"width\";i:300;s:6:\"height\";i:84;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(21, 21, '_wp_attached_file', '2019/03/assistance.png'),
(22, 21, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:96;s:6:\"height\";i:98;s:4:\"file\";s:22:\"2019/03/assistance.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(23, 22, '_wp_attached_file', '2019/03/back-footer.png'),
(24, 22, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1915;s:6:\"height\";i:941;s:4:\"file\";s:23:\"2019/03/back-footer.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"back-footer-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"back-footer-300x147.png\";s:5:\"width\";i:300;s:6:\"height\";i:147;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"back-footer-768x377.png\";s:5:\"width\";i:768;s:6:\"height\";i:377;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:24:\"back-footer-1024x503.png\";s:5:\"width\";i:1024;s:6:\"height\";i:503;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(25, 23, '_wp_attached_file', '2019/03/banner.jpg'),
(26, 23, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1366;s:6:\"height\";i:768;s:4:\"file\";s:18:\"2019/03/banner.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"banner-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"banner-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"banner-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"banner-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(27, 24, '_wp_attached_file', '2019/03/banner2.jpeg'),
(28, 24, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:333;s:4:\"file\";s:20:\"2019/03/banner2.jpeg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"banner2-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"banner2-300x200.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(29, 25, '_wp_attached_file', '2019/03/banner3.jpg'),
(30, 25, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1280;s:6:\"height\";i:720;s:4:\"file\";s:19:\"2019/03/banner3.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"banner3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"banner3-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"banner3-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"banner3-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(31, 26, '_wp_attached_file', '2019/03/banner4.jpg'),
(32, 26, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:300;s:6:\"height\";i:168;s:4:\"file\";s:19:\"2019/03/banner4.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"banner4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"banner4-300x168.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:168;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(33, 27, '_wp_attached_file', '2019/03/banner5.jpg'),
(34, 27, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:3840;s:6:\"height\";i:2160;s:4:\"file\";s:19:\"2019/03/banner5.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"banner5-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"banner5-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"banner5-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"banner5-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(35, 28, '_wp_attached_file', '2019/03/flecha-direita.png'),
(36, 28, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:480;s:6:\"height\";i:480;s:4:\"file\";s:26:\"2019/03/flecha-direita.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"flecha-direita-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"flecha-direita-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(37, 29, '_wp_attached_file', '2019/03/flecha-esquerda.png'),
(38, 29, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:480;s:6:\"height\";i:480;s:4:\"file\";s:27:\"2019/03/flecha-esquerda.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"flecha-esquerda-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"flecha-esquerda-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(39, 30, '_wp_attached_file', '2019/03/foto-sobre.png'),
(40, 30, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:623;s:6:\"height\";i:662;s:4:\"file\";s:22:\"2019/03/foto-sobre.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"foto-sobre-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"foto-sobre-282x300.png\";s:5:\"width\";i:282;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(41, 31, '_wp_attached_file', '2019/03/group.png'),
(42, 31, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:96;s:6:\"height\";i:98;s:4:\"file\";s:17:\"2019/03/group.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(43, 32, '_wp_attached_file', '2019/03/house.png'),
(44, 32, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:96;s:6:\"height\";i:98;s:4:\"file\";s:17:\"2019/03/house.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(71, 46, '_edit_last', '1'),
(59, 40, '_edit_last', '1'),
(60, 40, '_edit_lock', '1552724181:1'),
(61, 40, '_thumbnail_id', '32'),
(62, 41, '_edit_last', '1'),
(63, 41, '_edit_lock', '1552747738:1'),
(64, 41, '_thumbnail_id', '21'),
(65, 43, '_edit_last', '1'),
(66, 43, '_edit_lock', '1552747740:1'),
(67, 43, '_thumbnail_id', '31'),
(68, 44, '_edit_last', '1'),
(69, 44, '_edit_lock', '1552747738:1'),
(70, 44, '_thumbnail_id', '36'),
(47, 34, '_wp_attached_file', '2019/03/message.png'),
(48, 34, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:390;s:4:\"file\";s:19:\"2019/03/message.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"message-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"message-300x229.png\";s:5:\"width\";i:300;s:6:\"height\";i:229;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(49, 35, '_wp_attached_file', '2019/03/message-contato.png'),
(50, 35, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:480;s:6:\"height\";i:344;s:4:\"file\";s:27:\"2019/03/message-contato.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"message-contato-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"message-contato-300x215.png\";s:5:\"width\";i:300;s:6:\"height\";i:215;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(51, 36, '_wp_attached_file', '2019/03/paper.png'),
(52, 36, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:96;s:6:\"height\";i:98;s:4:\"file\";s:17:\"2019/03/paper.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(53, 37, '_wp_attached_file', '2019/03/phone.png'),
(54, 37, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:512;s:4:\"file\";s:17:\"2019/03/phone.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"phone-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"phone-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(55, 38, '_wp_attached_file', '2019/03/phone-contato.png'),
(56, 38, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:512;s:4:\"file\";s:25:\"2019/03/phone-contato.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"phone-contato-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"phone-contato-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(57, 39, '_wp_attached_file', '2019/03/photo.png'),
(58, 39, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:55;s:6:\"height\";i:56;s:4:\"file\";s:17:\"2019/03/photo.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(72, 46, '_edit_lock', '1552767948:1'),
(274, 46, '_thumbnail_id', '67'),
(74, 47, '_edit_last', '1'),
(75, 47, '_edit_lock', '1552767946:1'),
(76, 47, '_thumbnail_id', '53'),
(77, 48, '_edit_last', '1'),
(78, 48, '_edit_lock', '1552767945:1'),
(79, 48, '_thumbnail_id', '61'),
(80, 46, '_wp_old_slug', 'apartamento'),
(81, 48, 'MarianaGravon_foto_mob', '27'),
(82, 48, 'MarianaGravon_foto_mob', '26'),
(83, 48, 'MarianaGravon_foto_mob', '25'),
(84, 48, 'MarianaGravon_foto_mob', '23'),
(85, 48, 'MarianaGravon_foto_mob', '24'),
(86, 49, '_edit_last', '1'),
(87, 49, '_edit_lock', '1552747739:1'),
(88, 50, '_wp_attached_file', '2019/03/02_Img.jpg'),
(89, 50, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1600;s:6:\"height\";i:1200;s:4:\"file\";s:18:\"2019/03/02_Img.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"02_Img-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"02_Img-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"02_Img-768x576.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"02_Img-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(90, 51, '_wp_attached_file', '2019/03/03_Img.jpg'),
(91, 51, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1600;s:6:\"height\";i:1200;s:4:\"file\";s:18:\"2019/03/03_Img.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"03_Img-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"03_Img-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"03_Img-768x576.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"03_Img-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(92, 52, '_wp_attached_file', '2019/03/04_Img-bwc.jpg'),
(93, 52, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1600;s:6:\"height\";i:1200;s:4:\"file\";s:22:\"2019/03/04_Img-bwc.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"04_Img-bwc-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"04_Img-bwc-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"04_Img-bwc-768x576.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:23:\"04_Img-bwc-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(94, 53, '_wp_attached_file', '2019/03/1312_Quarto-01.jpg'),
(95, 53, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1280;s:6:\"height\";i:720;s:4:\"file\";s:26:\"2019/03/1312_Quarto-01.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"1312_Quarto-01-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"1312_Quarto-01-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"1312_Quarto-01-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:27:\"1312_Quarto-01-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(96, 54, '_wp_attached_file', '2019/03/1312_Quarto-02.jpg'),
(97, 54, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1280;s:6:\"height\";i:720;s:4:\"file\";s:26:\"2019/03/1312_Quarto-02.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"1312_Quarto-02-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"1312_Quarto-02-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"1312_Quarto-02-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:27:\"1312_Quarto-02-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(98, 55, '_wp_attached_file', '2019/03/1312_Quarto-03.jpg'),
(99, 55, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1280;s:6:\"height\";i:720;s:4:\"file\";s:26:\"2019/03/1312_Quarto-03.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"1312_Quarto-03-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"1312_Quarto-03-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"1312_Quarto-03-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:27:\"1312_Quarto-03-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(100, 56, '_wp_attached_file', '2019/03/1614_Op-4-Pers-cena-1.jpg'),
(101, 56, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2048;s:6:\"height\";i:1536;s:4:\"file\";s:33:\"2019/03/1614_Op-4-Pers-cena-1.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"1614_Op-4-Pers-cena-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"1614_Op-4-Pers-cena-1-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"1614_Op-4-Pers-cena-1-768x576.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:34:\"1614_Op-4-Pers-cena-1-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(102, 57, '_wp_attached_file', '2019/03/1614_Op-4-Pers-cena-2.jpg'),
(103, 57, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2048;s:6:\"height\";i:1536;s:4:\"file\";s:33:\"2019/03/1614_Op-4-Pers-cena-2.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"1614_Op-4-Pers-cena-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"1614_Op-4-Pers-cena-2-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"1614_Op-4-Pers-cena-2-768x576.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:34:\"1614_Op-4-Pers-cena-2-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(104, 58, '_wp_attached_file', '2019/03/1830_Op2-Lilian-Img1.jpg'),
(105, 58, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1600;s:6:\"height\";i:1200;s:4:\"file\";s:32:\"2019/03/1830_Op2-Lilian-Img1.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"1830_Op2-Lilian-Img1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"1830_Op2-Lilian-Img1-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"1830_Op2-Lilian-Img1-768x576.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:33:\"1830_Op2-Lilian-Img1-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(106, 59, '_wp_attached_file', '2019/03/1830_Op2-Lilian-Img2.jpg'),
(107, 59, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1080;s:4:\"file\";s:32:\"2019/03/1830_Op2-Lilian-Img2.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"1830_Op2-Lilian-Img2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"1830_Op2-Lilian-Img2-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"1830_Op2-Lilian-Img2-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:33:\"1830_Op2-Lilian-Img2-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(108, 60, '_wp_attached_file', '2019/03/1830_Op2-Lilian-Img3.jpg'),
(109, 60, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1080;s:4:\"file\";s:32:\"2019/03/1830_Op2-Lilian-Img3.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"1830_Op2-Lilian-Img3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"1830_Op2-Lilian-Img3-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"1830_Op2-Lilian-Img3-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:33:\"1830_Op2-Lilian-Img3-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(110, 61, '_wp_attached_file', '2019/03/1830_Op2-Lilian-Img4.jpg'),
(111, 61, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1080;s:4:\"file\";s:32:\"2019/03/1830_Op2-Lilian-Img4.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"1830_Op2-Lilian-Img4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"1830_Op2-Lilian-Img4-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"1830_Op2-Lilian-Img4-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:33:\"1830_Op2-Lilian-Img4-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(112, 62, '_wp_attached_file', '2019/03/1830_Op2-Lilian-Img5.jpg'),
(113, 62, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1080;s:4:\"file\";s:32:\"2019/03/1830_Op2-Lilian-Img5.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"1830_Op2-Lilian-Img5-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"1830_Op2-Lilian-Img5-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"1830_Op2-Lilian-Img5-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:33:\"1830_Op2-Lilian-Img5-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(114, 63, '_wp_attached_file', '2019/03/1830_Op2-Lilian-Img6.jpg'),
(115, 63, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1080;s:4:\"file\";s:32:\"2019/03/1830_Op2-Lilian-Img6.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"1830_Op2-Lilian-Img6-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"1830_Op2-Lilian-Img6-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"1830_Op2-Lilian-Img6-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:33:\"1830_Op2-Lilian-Img6-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(116, 64, '_wp_attached_file', '2019/03/1837_Churrasqueira-01.jpg'),
(117, 64, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1280;s:6:\"height\";i:720;s:4:\"file\";s:33:\"2019/03/1837_Churrasqueira-01.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"1837_Churrasqueira-01-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"1837_Churrasqueira-01-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"1837_Churrasqueira-01-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:34:\"1837_Churrasqueira-01-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(118, 65, '_wp_attached_file', '2019/03/1837_Churrasqueira-012.jpg'),
(119, 65, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1280;s:6:\"height\";i:720;s:4:\"file\";s:34:\"2019/03/1837_Churrasqueira-012.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"1837_Churrasqueira-012-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"1837_Churrasqueira-012-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:34:\"1837_Churrasqueira-012-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:35:\"1837_Churrasqueira-012-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(120, 66, '_wp_attached_file', '2019/03/1837_Churrasqueira.jpg'),
(121, 66, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1280;s:6:\"height\";i:720;s:4:\"file\";s:30:\"2019/03/1837_Churrasqueira.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"1837_Churrasqueira-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"1837_Churrasqueira-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"1837_Churrasqueira-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:31:\"1837_Churrasqueira-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(122, 67, '_wp_attached_file', '2019/03/1837_Churrasqueira7.jpg'),
(123, 67, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1280;s:6:\"height\";i:720;s:4:\"file\";s:31:\"2019/03/1837_Churrasqueira7.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"1837_Churrasqueira7-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"1837_Churrasqueira7-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"1837_Churrasqueira7-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:32:\"1837_Churrasqueira7-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(124, 68, '_wp_attached_file', '2019/03/1837_I.s.-Hóspedes-01.jpg'),
(125, 68, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1280;s:6:\"height\";i:720;s:4:\"file\";s:34:\"2019/03/1837_I.s.-Hóspedes-01.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"1837_I.s.-Hóspedes-01-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"1837_I.s.-Hóspedes-01-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:34:\"1837_I.s.-Hóspedes-01-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:35:\"1837_I.s.-Hóspedes-01-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(126, 69, '_wp_attached_file', '2019/03/1837_I.s.-Hóspedes-02.jpg'),
(127, 69, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1280;s:6:\"height\";i:720;s:4:\"file\";s:34:\"2019/03/1837_I.s.-Hóspedes-02.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"1837_I.s.-Hóspedes-02-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"1837_I.s.-Hóspedes-02-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:34:\"1837_I.s.-Hóspedes-02-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:35:\"1837_I.s.-Hóspedes-02-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(128, 70, '_wp_attached_file', '2019/03/1837_I.s.-Hóspedes-03.jpg'),
(129, 70, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1280;s:6:\"height\";i:720;s:4:\"file\";s:34:\"2019/03/1837_I.s.-Hóspedes-03.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"1837_I.s.-Hóspedes-03-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"1837_I.s.-Hóspedes-03-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:34:\"1837_I.s.-Hóspedes-03-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:35:\"1837_I.s.-Hóspedes-03-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(130, 71, '_wp_attached_file', '2019/03/1837_Suíte-Casal-Op.2-01.jpg'),
(131, 71, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1280;s:6:\"height\";i:720;s:4:\"file\";s:37:\"2019/03/1837_Suíte-Casal-Op.2-01.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"1837_Suíte-Casal-Op.2-01-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:37:\"1837_Suíte-Casal-Op.2-01-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:37:\"1837_Suíte-Casal-Op.2-01-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:38:\"1837_Suíte-Casal-Op.2-01-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(132, 72, '_wp_attached_file', '2019/03/1837_Suíte-Casal-Op.2-02.jpg'),
(133, 72, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1280;s:6:\"height\";i:720;s:4:\"file\";s:37:\"2019/03/1837_Suíte-Casal-Op.2-02.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"1837_Suíte-Casal-Op.2-02-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:37:\"1837_Suíte-Casal-Op.2-02-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:37:\"1837_Suíte-Casal-Op.2-02-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:38:\"1837_Suíte-Casal-Op.2-02-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(134, 73, '_wp_attached_file', '2019/03/1837_Suíte-Casal-Op.2-03.jpg'),
(135, 73, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1280;s:6:\"height\";i:720;s:4:\"file\";s:37:\"2019/03/1837_Suíte-Casal-Op.2-03.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"1837_Suíte-Casal-Op.2-03-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:37:\"1837_Suíte-Casal-Op.2-03-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:37:\"1837_Suíte-Casal-Op.2-03-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:38:\"1837_Suíte-Casal-Op.2-03-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(136, 74, '_wp_attached_file', '2019/03/1837_Suíte-Casal-Op.2-04.jpg'),
(137, 74, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1280;s:6:\"height\";i:720;s:4:\"file\";s:37:\"2019/03/1837_Suíte-Casal-Op.2-04.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"1837_Suíte-Casal-Op.2-04-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:37:\"1837_Suíte-Casal-Op.2-04-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:37:\"1837_Suíte-Casal-Op.2-04-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:38:\"1837_Suíte-Casal-Op.2-04-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(138, 49, '_thumbnail_id', '65'),
(139, 49, 'MarianaGravon_link_destaque', '#'),
(140, 75, '_edit_last', '1'),
(141, 75, '_edit_lock', '1552747739:1'),
(142, 75, '_thumbnail_id', '50'),
(143, 76, '_edit_last', '1'),
(144, 76, '_edit_lock', '1552747740:1'),
(145, 76, '_thumbnail_id', '51'),
(146, 77, '_edit_last', '1'),
(147, 77, '_edit_lock', '1552725257:1'),
(148, 77, '_thumbnail_id', '67'),
(288, 48, 'MarianaGravon_fotos_projeto', '64'),
(287, 48, 'MarianaGravon_fotos_projeto', '53'),
(286, 48, 'MarianaGravon_fotos_projeto', '54'),
(292, 47, 'MarianaGravon_fotos_projeto', '62'),
(291, 47, 'MarianaGravon_fotos_projeto', '63'),
(290, 47, 'MarianaGravon_fotos_projeto', '65'),
(279, 46, 'MarianaGravon_fotos_projeto', '58'),
(278, 46, 'MarianaGravon_fotos_projeto', '62'),
(277, 46, 'MarianaGravon_fotos_projeto', '61'),
(276, 46, 'MarianaGravon_fotos_projeto', '60'),
(162, 44, 'MarianaGravon_descricao_servico', 'Projeto de construção ou reforma baseada na necessidade de cada cliente e possibilidades do terreno, em função da sua tipologia e da legislação. É realizado um estudo de viabilidade e deito um programa'),
(163, 43, 'MarianaGravon_descricao_servico', 'Projeto de construção ou reforma baseada na necessidade de cada cliente e possibilidades do terreno, em função da sua tipologia e da legislação. É realizado um estudo de viabilidade e deito um programa'),
(164, 41, 'MarianaGravon_descricao_servico', 'Projeto de construção ou reforma baseada na necessidade de cada cliente e possibilidades do terreno, em função da sua tipologia e da legislação. É realizado um estudo de viabilidade e deito um programa'),
(165, 40, 'MarianaGravon_descricao_servico', 'Projeto de construção ou reforma baseada na necessidade de cada cliente e possibilidades do terreno, em função da sua tipologia e da legislação. É realizado um estudo de viabilidade e deito um programa'),
(166, 77, 'MarianaGravon_checkebox_destaque', '1'),
(167, 76, 'MarianaGravon_checkebox_destaque', '1'),
(168, 75, 'MarianaGravon_checkebox_destaque', '1'),
(169, 49, 'MarianaGravon_checkebox_destaque', '1'),
(170, 16, '_wp_trash_meta_status', 'publish'),
(171, 16, '_wp_trash_meta_time', '1552725414'),
(172, 16, '_wp_desired_post_slug', 'portfolio'),
(173, 12, '_wp_trash_meta_status', 'publish');
INSERT INTO `mg_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(174, 12, '_wp_trash_meta_time', '1552725418'),
(175, 12, '_wp_desired_post_slug', 'servicos'),
(176, 78, '_menu_item_type', 'post_type'),
(177, 78, '_menu_item_menu_item_parent', '0'),
(178, 78, '_menu_item_object_id', '14'),
(179, 78, '_menu_item_object', 'page'),
(180, 78, '_menu_item_target', ''),
(181, 78, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(182, 78, '_menu_item_xfn', ''),
(183, 78, '_menu_item_url', ''),
(185, 79, '_menu_item_type', 'post_type'),
(186, 79, '_menu_item_menu_item_parent', '0'),
(187, 79, '_menu_item_object_id', '9'),
(188, 79, '_menu_item_object', 'page'),
(189, 79, '_menu_item_target', ''),
(190, 79, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(191, 79, '_menu_item_xfn', ''),
(192, 79, '_menu_item_url', ''),
(194, 80, '_menu_item_type', 'post_type'),
(195, 80, '_menu_item_menu_item_parent', '0'),
(196, 80, '_menu_item_object_id', '6'),
(197, 80, '_menu_item_object', 'page'),
(198, 80, '_menu_item_target', ''),
(199, 80, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(200, 80, '_menu_item_xfn', ''),
(201, 80, '_menu_item_url', ''),
(203, 81, '_form', '<label for=\"nome\">Seu nome</label>[text* nome id:nome class:nome placeholder \"Seu nome\"]<label for=\"email\">E-mail</label>[email* email id:email class:email placeholder \"E-mail\"]<label for=\"mensagem\">Mensagem</label>[textarea projeto id:projeto class:projeto placeholder \"Qual é seu projeto?\"]<div class=\"btnSubmitFormContato\">[submit id:enviar class:enviar \"Entrar em contato\"]</div>'),
(204, 81, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:43:\"Mariana Gravon Arquitetura \"[your-subject]\"\";s:6:\"sender\";s:60:\"Mariana Gravon Arquitetura <devhcdesenvolvimentos@gmail.com>\";s:9:\"recipient\";s:31:\"devhcdesenvolvimentos@gmail.com\";s:4:\"body\";s:209:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Mariana Gravon Arquitetura (http://localhost/projetos/marianagravon_site)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(205, 81, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:43:\"Mariana Gravon Arquitetura \"[your-subject]\"\";s:6:\"sender\";s:60:\"Mariana Gravon Arquitetura <devhcdesenvolvimentos@gmail.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:151:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Mariana Gravon Arquitetura (http://localhost/projetos/marianagravon_site)\";s:18:\"additional_headers\";s:41:\"Reply-To: devhcdesenvolvimentos@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(206, 81, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(207, 81, '_additional_settings', ''),
(208, 81, '_locale', 'pt_BR'),
(210, 81, '_config_errors', 'a:1:{s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:51:\"Invalid mailbox syntax is used in the %name% field.\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(215, 48, 'MarianaGravon_desc_projeto', 'Cliente3 - Data - Tipo'),
(220, 47, 'MarianaGravon_desc_projeto', 'Cliente2 - Data - Tipo'),
(226, 46, 'MarianaGravon_desc_projeto', 'Cliente1 - Data - Tipo'),
(285, 48, 'MarianaGravon_fotos_projeto', '55'),
(289, 47, 'MarianaGravon_fotos_projeto', '66'),
(275, 46, 'MarianaGravon_fotos_projeto', '59'),
(240, 82, '_menu_item_type', 'custom'),
(241, 82, '_menu_item_menu_item_parent', '0'),
(242, 82, '_menu_item_object_id', '82'),
(243, 82, '_menu_item_object', 'custom'),
(244, 82, '_menu_item_target', ''),
(245, 82, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(246, 82, '_menu_item_xfn', ''),
(247, 82, '_menu_item_url', 'http://localhost/projetos/marianagravon_site/portfolio'),
(280, 46, 'MarianaGravon_fotos_projeto', '64'),
(249, 83, '_menu_item_type', 'custom'),
(250, 83, '_menu_item_menu_item_parent', '0'),
(251, 83, '_menu_item_object_id', '83'),
(252, 83, '_menu_item_object', 'custom'),
(253, 83, '_menu_item_target', ''),
(254, 83, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(255, 83, '_menu_item_xfn', ''),
(256, 83, '_menu_item_url', 'http://localhost/projetos/marianagravon_site/servicos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `mg_posts`
--

DROP TABLE IF EXISTS `mg_posts`;
CREATE TABLE IF NOT EXISTS `mg_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=MyISAM AUTO_INCREMENT=84 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `mg_posts`
--

INSERT INTO `mg_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-03-14 16:32:23', '2019-03-14 19:32:23', '<!-- wp:paragraph -->\n<p>Boas-vindas ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!</p>\n<!-- /wp:paragraph -->', 'Olá, mundo!', '', 'publish', 'open', 'open', '', 'ola-mundo', '', '', '2019-03-14 16:32:23', '2019-03-14 19:32:23', '', 0, 'http://localhost/projetos/marianagravon_site/?p=1', 0, 'post', '', 1),
(2, 1, '2019-03-14 16:32:23', '2019-03-14 19:32:23', '<!-- wp:paragraph -->\n<p>Esta é uma página de exemplo. É diferente de um post no blog porque ela permanecerá em um lugar e aparecerá na navegação do seu site na maioria dos temas. Muitas pessoas começam com uma página que as apresenta a possíveis visitantes do site. Ela pode dizer algo assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Olá! Eu sou um mensageiro de bicicleta durante o dia, ator aspirante à noite, e este é o meu site. Eu moro em São Paulo, tenho um grande cachorro chamado Rex e gosto de tomar caipirinha (e banhos de chuva).</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...ou alguma coisa assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>A Companhia de Miniaturas XYZ foi fundada em 1971, e desde então tem fornecido miniaturas de qualidade ao público. Localizada na cidade de Itu, a XYZ emprega mais de 2.000 pessoas e faz coisas grandiosas para a comunidade da cidade.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Como um novo usuário do WordPress, você deveria ir ao <a href=\"http://localhost/projetos/marianagravon_site/wp-admin/\">painel</a> para excluir essa página e criar novas páginas para o seu conteúdo. Divirta-se!</p>\n<!-- /wp:paragraph -->', 'Página de exemplo', '', 'trash', 'closed', 'open', '', 'pagina-exemplo__trashed', '', '', '2019-03-14 16:34:57', '2019-03-14 19:34:57', '', 0, 'http://localhost/projetos/marianagravon_site/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-03-14 16:32:23', '2019-03-14 19:32:23', '<!-- wp:heading --><h2>Quem somos</h2><!-- /wp:heading --><!-- wp:paragraph --><p>O endereço do nosso site é: http://localhost/projetos/marianagravon_site.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais dados pessoais coletamos e porque</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comentários</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Quando os visitantes deixam comentários no site, coletamos os dados mostrados no formulário de comentários, além do endereço de IP e de dados do navegador do visitante, para auxiliar na detecção de spam.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Uma sequência anonimizada de caracteres criada a partir do seu e-mail (também chamada de hash) poderá ser enviada para o Gravatar para verificar se você usa o serviço. A política de privacidade do Gravatar está disponível aqui: https://automattic.com/privacy/. Depois da aprovação do seu comentário, a foto do seu perfil fica visível publicamente junto de seu comentário.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Se você envia imagens para o site, evite enviar as que contenham dados de localização incorporados (EXIF GPS). Visitantes podem baixar estas imagens do site e extrair delas seus dados de localização.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Formulários de contato</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Ao deixar um comentário no site, você poderá optar por salvar seu nome, e-mail e site nos cookies. Isso visa seu conforto, assim você não precisará preencher seus  dados novamente quando fizer outro comentário. Estes cookies duram um ano.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você tem uma conta e acessa este site, um cookie temporário será criado para determinar se seu navegador aceita cookies. Ele não contém nenhum dado pessoal e será descartado quando você fechar seu navegador.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Quando você acessa sua conta no site, também criamos vários cookies para salvar os dados da sua conta e suas escolhas de exibição de tela. Cookies de login são mantidos por dois dias e cookies de opções de tela por um ano. Se você selecionar &quot;Lembrar-me&quot;, seu acesso será mantido por duas semanas. Se você se desconectar da sua conta, os cookies de login serão removidos.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você editar ou publicar um artigo, um cookie adicional será salvo no seu navegador. Este cookie não inclui nenhum dado pessoal e simplesmente indica o ID do post referente ao artigo que você acabou de editar. Ele expira depois de 1 dia.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia incorporada de outros sites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Artigos neste site podem incluir conteúdo incorporado como, por exemplo, vídeos, imagens, artigos, etc. Conteúdos incorporados de outros sites se comportam exatamente da mesma forma como se o visitante estivesse visitando o outro site.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Estes sites podem coletar dados sobre você, usar cookies, incorporar rastreamento adicional de terceiros e monitorar sua interação com este conteúdo incorporado, incluindo sua interação com o conteúdo incorporado se você tem uma conta e está conectado com o site.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Análises</h3><!-- /wp:heading --><!-- wp:heading --><h2>Com quem partilhamos seus dados</h2><!-- /wp:heading --><!-- wp:heading --><h2>Por quanto tempo mantemos os seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você deixar um comentário, o comentário e os seus metadados são conservados indefinidamente. Fazemos isso para que seja possível reconhecer e aprovar automaticamente qualquer comentário posterior ao invés de retê-lo para moderação.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Para usuários que se registram no nosso site (se houver), também guardamos as informações pessoais que fornecem no seu perfil de usuário. Todos os usuários podem ver, editar ou excluir suas informações pessoais a qualquer momento (só não é possível alterar o seu username). Os administradores de sites também podem ver e editar estas informações.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais os seus direitos sobre seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você tiver uma conta neste site ou se tiver deixado comentários, pode solicitar um arquivo exportado dos dados pessoais que mantemos sobre você, inclusive quaisquer dados que nos tenha fornecido. Também pode solicitar que removamos qualquer dado pessoal que mantemos sobre você. Isto não inclui nenhuns dados que somos obrigados a manter para propósitos administrativos, legais ou de segurança.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Para onde enviamos seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Comentários de visitantes podem ser marcados por um serviço automático de detecção de spam.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Suas informações de contato</h2><!-- /wp:heading --><!-- wp:heading --><h2>Informações adicionais</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Como protegemos seus dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais são nossos procedimentos contra violação de dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>De quais terceiros nós recebemos dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais tomadas de decisão ou análises de perfil automatizadas fazemos com os dados de usuários</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Requisitos obrigatórios de divulgação para sua categoria profissional</h3><!-- /wp:heading -->', 'Política de privacidade', '', 'draft', 'closed', 'open', '', 'politica-de-privacidade', '', '', '2019-03-14 16:32:23', '2019-03-14 19:32:23', '', 0, 'http://localhost/projetos/marianagravon_site/?page_id=3', 0, 'page', '', 0),
(4, 1, '2019-03-14 16:32:45', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-03-14 16:32:45', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/marianagravon_site/?p=4', 0, 'post', '', 0),
(5, 1, '2019-03-14 16:34:57', '2019-03-14 19:34:57', '<!-- wp:paragraph -->\n<p>Esta é uma página de exemplo. É diferente de um post no blog porque ela permanecerá em um lugar e aparecerá na navegação do seu site na maioria dos temas. Muitas pessoas começam com uma página que as apresenta a possíveis visitantes do site. Ela pode dizer algo assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Olá! Eu sou um mensageiro de bicicleta durante o dia, ator aspirante à noite, e este é o meu site. Eu moro em São Paulo, tenho um grande cachorro chamado Rex e gosto de tomar caipirinha (e banhos de chuva).</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...ou alguma coisa assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>A Companhia de Miniaturas XYZ foi fundada em 1971, e desde então tem fornecido miniaturas de qualidade ao público. Localizada na cidade de Itu, a XYZ emprega mais de 2.000 pessoas e faz coisas grandiosas para a comunidade da cidade.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Como um novo usuário do WordPress, você deveria ir ao <a href=\"http://localhost/projetos/marianagravon_site/wp-admin/\">painel</a> para excluir essa página e criar novas páginas para o seu conteúdo. Divirta-se!</p>\n<!-- /wp:paragraph -->', 'Página de exemplo', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2019-03-14 16:34:57', '2019-03-14 19:34:57', '', 2, 'http://localhost/projetos/marianagravon_site/2019/03/14/2-revision-v1/', 0, 'revision', '', 0),
(6, 1, '2019-03-14 16:35:16', '2019-03-14 19:35:16', '', 'Inicial', '', 'publish', 'closed', 'closed', '', 'inicial', '', '', '2019-03-14 16:53:13', '2019-03-14 19:53:13', '', 0, 'http://localhost/projetos/marianagravon_site/?page_id=6', 0, 'page', '', 0),
(7, 1, '2019-03-14 16:35:16', '2019-03-14 19:35:16', '', 'Inicial', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2019-03-14 16:35:16', '2019-03-14 19:35:16', '', 6, 'http://localhost/projetos/marianagravon_site/2019/03/14/6-revision-v1/', 0, 'revision', '', 0),
(8, 1, '2019-03-15 09:00:31', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-03-15 09:00:31', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/marianagravon_site/?page_id=8', 0, 'page', '', 0),
(9, 1, '2019-03-15 09:02:44', '2019-03-15 12:02:44', '', 'Sobre', '', 'publish', 'closed', 'closed', '', 'sobre', '', '', '2019-03-15 09:02:44', '2019-03-15 12:02:44', '', 0, 'http://localhost/projetos/marianagravon_site/?page_id=9', 0, 'page', '', 0),
(10, 1, '2019-03-15 09:02:44', '2019-03-15 12:02:44', '', 'Sobre', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2019-03-15 09:02:44', '2019-03-15 12:02:44', '', 9, 'http://localhost/projetos/marianagravon_site/2019/03/15/9-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2019-03-15 09:02:50', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-03-15 09:02:50', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/marianagravon_site/?page_id=11', 0, 'page', '', 0),
(12, 1, '2019-03-15 09:07:23', '2019-03-15 12:07:23', '', 'Serviços', '', 'trash', 'closed', 'closed', '', 'servicos__trashed', '', '', '2019-03-16 05:36:58', '2019-03-16 08:36:58', '', 0, 'http://localhost/projetos/marianagravon_site/?page_id=12', 0, 'page', '', 0),
(13, 1, '2019-03-15 09:07:23', '2019-03-15 12:07:23', '', 'Serviços', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2019-03-15 09:07:23', '2019-03-15 12:07:23', '', 12, 'http://localhost/projetos/marianagravon_site/2019/03/15/12-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2019-03-15 09:07:37', '2019-03-15 12:07:37', '', 'Contato', '', 'publish', 'closed', 'closed', '', 'contato', '', '', '2019-03-15 09:07:37', '2019-03-15 12:07:37', '', 0, 'http://localhost/projetos/marianagravon_site/?page_id=14', 0, 'page', '', 0),
(15, 1, '2019-03-15 09:07:37', '2019-03-15 12:07:37', '', 'Contato', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2019-03-15 09:07:37', '2019-03-15 12:07:37', '', 14, 'http://localhost/projetos/marianagravon_site/2019/03/15/14-revision-v1/', 0, 'revision', '', 0),
(16, 1, '2019-03-15 09:07:52', '2019-03-15 12:07:52', '', 'Portfolio', '', 'trash', 'closed', 'closed', '', 'portfolio__trashed', '', '', '2019-03-16 05:36:54', '2019-03-16 08:36:54', '', 0, 'http://localhost/projetos/marianagravon_site/?page_id=16', 0, 'page', '', 0),
(17, 1, '2019-03-15 09:07:52', '2019-03-15 12:07:52', '', 'Portfólio', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2019-03-15 09:07:52', '2019-03-15 12:07:52', '', 16, 'http://localhost/projetos/marianagravon_site/2019/03/15/16-revision-v1/', 0, 'revision', '', 0),
(18, 1, '2019-03-15 09:08:00', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-03-15 09:08:00', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/marianagravon_site/?page_id=18', 0, 'page', '', 0),
(19, 1, '2019-03-15 09:12:40', '2019-03-15 12:12:40', '', 'Portfolio', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2019-03-15 09:12:40', '2019-03-15 12:12:40', '', 16, 'http://localhost/projetos/marianagravon_site/2019/03/15/16-revision-v1/', 0, 'revision', '', 0),
(20, 1, '2019-03-15 10:41:22', '2019-03-15 13:41:22', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2019-03-15 10:41:22', '2019-03-15 13:41:22', '', 0, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/logo.png', 0, 'attachment', 'image/png', 0),
(21, 1, '2019-03-15 10:41:29', '2019-03-15 13:41:29', '', 'assistance', '', 'inherit', 'open', 'closed', '', 'assistance', '', '', '2019-03-15 10:41:29', '2019-03-15 13:41:29', '', 0, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/assistance.png', 0, 'attachment', 'image/png', 0),
(22, 1, '2019-03-15 10:41:30', '2019-03-15 13:41:30', '', 'back-footer', '', 'inherit', 'open', 'closed', '', 'back-footer', '', '', '2019-03-15 10:41:30', '2019-03-15 13:41:30', '', 0, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/back-footer.png', 0, 'attachment', 'image/png', 0),
(23, 1, '2019-03-15 10:41:31', '2019-03-15 13:41:31', '', 'banner', '', 'inherit', 'open', 'closed', '', 'banner', '', '', '2019-03-15 10:41:31', '2019-03-15 13:41:31', '', 0, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/banner.jpg', 0, 'attachment', 'image/jpeg', 0),
(24, 1, '2019-03-15 10:41:32', '2019-03-15 13:41:32', '', 'banner2', '', 'inherit', 'open', 'closed', '', 'banner2', '', '', '2019-03-15 10:41:32', '2019-03-15 13:41:32', '', 0, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/banner2.jpeg', 0, 'attachment', 'image/jpeg', 0),
(25, 1, '2019-03-15 10:41:33', '2019-03-15 13:41:33', '', 'banner3', '', 'inherit', 'open', 'closed', '', 'banner3', '', '', '2019-03-15 10:41:33', '2019-03-15 13:41:33', '', 0, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/banner3.jpg', 0, 'attachment', 'image/jpeg', 0),
(26, 1, '2019-03-15 10:41:33', '2019-03-15 13:41:33', '', 'banner4', '', 'inherit', 'open', 'closed', '', 'banner4', '', '', '2019-03-15 10:41:33', '2019-03-15 13:41:33', '', 0, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/banner4.jpg', 0, 'attachment', 'image/jpeg', 0),
(27, 1, '2019-03-15 10:41:34', '2019-03-15 13:41:34', '', 'banner5', '', 'inherit', 'open', 'closed', '', 'banner5', '', '', '2019-03-15 10:41:34', '2019-03-15 13:41:34', '', 0, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/banner5.jpg', 0, 'attachment', 'image/jpeg', 0),
(28, 1, '2019-03-15 10:41:36', '2019-03-15 13:41:36', '', 'flecha-direita', '', 'inherit', 'open', 'closed', '', 'flecha-direita', '', '', '2019-03-15 10:41:36', '2019-03-15 13:41:36', '', 0, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/flecha-direita.png', 0, 'attachment', 'image/png', 0),
(29, 1, '2019-03-15 10:41:36', '2019-03-15 13:41:36', '', 'flecha-esquerda', '', 'inherit', 'open', 'closed', '', 'flecha-esquerda', '', '', '2019-03-15 10:41:36', '2019-03-15 13:41:36', '', 0, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/flecha-esquerda.png', 0, 'attachment', 'image/png', 0),
(30, 1, '2019-03-15 10:41:37', '2019-03-15 13:41:37', '', 'foto-sobre', '', 'inherit', 'open', 'closed', '', 'foto-sobre', '', '', '2019-03-15 10:41:37', '2019-03-15 13:41:37', '', 0, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/foto-sobre.png', 0, 'attachment', 'image/png', 0),
(31, 1, '2019-03-15 10:41:37', '2019-03-15 13:41:37', '', 'group', '', 'inherit', 'open', 'closed', '', 'group', '', '', '2019-03-15 10:41:37', '2019-03-15 13:41:37', '', 0, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/group.png', 0, 'attachment', 'image/png', 0),
(32, 1, '2019-03-15 10:41:38', '2019-03-15 13:41:38', '', 'house', '', 'inherit', 'open', 'closed', '', 'house', '', '', '2019-03-15 10:41:38', '2019-03-15 13:41:38', '', 0, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/house.png', 0, 'attachment', 'image/png', 0),
(40, 1, '2019-03-15 11:42:36', '2019-03-15 14:42:36', '<p style=\"text-align: left;\">Projeto de construção ou reforma baseada na necessidade de cada cliente e possibilidades do terreno, em função da sua tipologia e da legislação. É realizado um estudo de viabilidade e deito um programa,onde o principal objetivo é traduzir a personalidade do cliente no espaço que será projetado.</p>\r\n\r\n<ul>\r\n 	<li>Estudo de variabilidade</li>\r\n 	<li>Concepção de novos projetos</li>\r\n 	<li>Reformas</li>\r\n 	<li>Execução de obras</li>\r\n</ul>', 'Arquitetura', '', 'publish', 'closed', 'closed', '', 'arquitetura', '', '', '2019-03-16 04:45:15', '2019-03-16 07:45:15', '', 0, 'http://localhost/projetos/marianagravon_site/?post_type=servico&#038;p=40', 0, 'servico', '', 0),
(34, 1, '2019-03-15 10:41:38', '2019-03-15 13:41:38', '', 'message', '', 'inherit', 'open', 'closed', '', 'message', '', '', '2019-03-15 10:41:38', '2019-03-15 13:41:38', '', 0, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/message.png', 0, 'attachment', 'image/png', 0),
(35, 1, '2019-03-15 10:41:39', '2019-03-15 13:41:39', '', 'message-contato', '', 'inherit', 'open', 'closed', '', 'message-contato', '', '', '2019-03-15 10:41:39', '2019-03-15 13:41:39', '', 0, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/message-contato.png', 0, 'attachment', 'image/png', 0),
(36, 1, '2019-03-15 10:41:39', '2019-03-15 13:41:39', '', 'paper', '', 'inherit', 'open', 'closed', '', 'paper', '', '', '2019-03-15 10:41:39', '2019-03-15 13:41:39', '', 0, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/paper.png', 0, 'attachment', 'image/png', 0),
(37, 1, '2019-03-15 10:41:40', '2019-03-15 13:41:40', '', 'phone', '', 'inherit', 'open', 'closed', '', 'phone', '', '', '2019-03-15 10:41:40', '2019-03-15 13:41:40', '', 0, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/phone.png', 0, 'attachment', 'image/png', 0),
(38, 1, '2019-03-15 10:41:40', '2019-03-15 13:41:40', '', 'phone-contato', '', 'inherit', 'open', 'closed', '', 'phone-contato', '', '', '2019-03-15 10:41:40', '2019-03-15 13:41:40', '', 0, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/phone-contato.png', 0, 'attachment', 'image/png', 0),
(39, 1, '2019-03-15 10:41:41', '2019-03-15 13:41:41', '', 'photo', '', 'inherit', 'open', 'closed', '', 'photo', '', '', '2019-03-15 10:41:41', '2019-03-15 13:41:41', '', 0, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/photo.png', 0, 'attachment', 'image/png', 0),
(41, 1, '2019-03-15 11:43:18', '2019-03-15 14:43:18', '<p style=\"text-align: left;\">O Projeto de Interiores planeja todos os detalhes de um ambiente pré-exis-stente com harmonia e técnica sempre procurando melhorar a função de cada espaço. Consiste na integração de elementos como iluminação, mobiliários e acabamentos.</p>\r\n\r\n<ul>\r\n 	<li>Desenvolvimento de Layout</li>\r\n 	<li>Detalhamento de móveis</li>\r\n 	<li>Especificação de matérias</li>\r\n 	<li>Execução</li>\r\n</ul>', 'Interiores', '', 'publish', 'closed', 'closed', '', 'interiores', '', '', '2019-03-16 04:45:13', '2019-03-16 07:45:13', '', 0, 'http://localhost/projetos/marianagravon_site/?post_type=servico&#038;p=41', 1, 'servico', '', 0),
(42, 1, '2019-03-15 11:43:22', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-03-15 11:43:22', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/marianagravon_site/?post_type=servico&p=42', 0, 'servico', '', 0),
(43, 1, '2019-03-15 11:43:58', '2019-03-15 14:43:58', '<p style=\"text-align: left;\">Orientação profissional com o objetivo de tirar dúvidas, dar sugestões e apresentar soluções que não consistam em proposta gráfica.</p>\r\n\r\n<ul>\r\n 	<li>Acompanhamento em lojas com auxílio na escolha de revestimentos, mobiliários e objetos</li>\r\n 	<li>Visitas a obras para sugestões de mudanças de lay-out</li>\r\n</ul>', 'Consultoria', '', 'publish', 'closed', 'closed', '', 'consultoria', '', '', '2019-03-16 04:45:11', '2019-03-16 07:45:11', '', 0, 'http://localhost/projetos/marianagravon_site/?post_type=servico&#038;p=43', 2, 'servico', '', 0),
(44, 1, '2019-03-15 11:44:21', '2019-03-15 14:44:21', '<ul>\r\n 	<li>Alvará de construção e Habite-se</li>\r\n 	<li>Aprovação de empreendimentos na Vigilância Sanitária (restaurantes, bares, clínicas, fábrica de alimentos, hotéis, etc)</li>\r\n</ul>', 'Legalização', '', 'publish', 'closed', 'closed', '', 'legalizacao', '', '', '2019-03-16 04:45:09', '2019-03-16 07:45:09', '', 0, 'http://localhost/projetos/marianagravon_site/?post_type=servico&#038;p=44', 3, 'servico', '', 0),
(45, 1, '2019-03-15 11:50:37', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-03-15 11:50:37', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/marianagravon_site/?post_type=portfolio&p=45', 0, 'portfolio', '', 0),
(46, 1, '2019-03-15 14:47:05', '2019-03-15 17:47:05', '', 'Sala', '', 'publish', 'closed', 'closed', '', 'sala', '', '', '2019-03-16 17:26:47', '2019-03-16 20:26:47', '', 0, 'http://localhost/projetos/marianagravon_site/?post_type=portfolio&#038;p=46', 0, 'portfolio', '', 0),
(47, 1, '2019-03-15 14:47:24', '2019-03-15 17:47:24', '', 'Casa', '', 'publish', 'closed', 'closed', '', 'casa', '', '', '2019-03-16 17:27:38', '2019-03-16 20:27:38', '', 0, 'http://localhost/projetos/marianagravon_site/?post_type=portfolio&#038;p=47', 0, 'portfolio', '', 0),
(48, 1, '2019-03-15 14:48:05', '2019-03-15 17:48:05', '', 'Prédio', '', 'publish', 'closed', 'closed', '', 'predio', '', '', '2019-03-16 17:27:24', '2019-03-16 20:27:24', '', 0, 'http://localhost/projetos/marianagravon_site/?post_type=portfolio&#038;p=48', 0, 'portfolio', '', 0),
(49, 1, '2019-03-16 04:07:36', '2019-03-16 07:07:36', '', 'Destaque 1', '', 'publish', 'closed', 'closed', '', 'destaque-1', '', '', '2019-03-16 05:25:45', '2019-03-16 08:25:45', '', 0, 'http://localhost/projetos/marianagravon_site/?post_type=destaque&#038;p=49', 0, 'destaque', '', 0),
(50, 1, '2019-03-16 04:02:17', '2019-03-16 07:02:17', '', '02_Img', '', 'inherit', 'open', 'closed', '', '02_img', '', '', '2019-03-16 04:02:17', '2019-03-16 07:02:17', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/02_Img.jpg', 0, 'attachment', 'image/jpeg', 0),
(51, 1, '2019-03-16 04:02:20', '2019-03-16 07:02:20', '', '03_Img', '', 'inherit', 'open', 'closed', '', '03_img', '', '', '2019-03-16 04:02:20', '2019-03-16 07:02:20', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/03_Img.jpg', 0, 'attachment', 'image/jpeg', 0),
(52, 1, '2019-03-16 04:02:23', '2019-03-16 07:02:23', '', '04_Img bwc', '', 'inherit', 'open', 'closed', '', '04_img-bwc', '', '', '2019-03-16 04:02:23', '2019-03-16 07:02:23', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/04_Img-bwc.jpg', 0, 'attachment', 'image/jpeg', 0),
(53, 1, '2019-03-16 04:02:27', '2019-03-16 07:02:27', '', '1312_Quarto 01', '', 'inherit', 'open', 'closed', '', '1312_quarto-01', '', '', '2019-03-16 04:02:27', '2019-03-16 07:02:27', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1312_Quarto-01.jpg', 0, 'attachment', 'image/jpeg', 0),
(54, 1, '2019-03-16 04:02:29', '2019-03-16 07:02:29', '', '1312_Quarto 02', '', 'inherit', 'open', 'closed', '', '1312_quarto-02', '', '', '2019-03-16 04:02:29', '2019-03-16 07:02:29', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1312_Quarto-02.jpg', 0, 'attachment', 'image/jpeg', 0),
(55, 1, '2019-03-16 04:02:32', '2019-03-16 07:02:32', '', '1312_Quarto 03', '', 'inherit', 'open', 'closed', '', '1312_quarto-03', '', '', '2019-03-16 04:02:32', '2019-03-16 07:02:32', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1312_Quarto-03.jpg', 0, 'attachment', 'image/jpeg', 0),
(56, 1, '2019-03-16 04:02:34', '2019-03-16 07:02:34', '', '1614_Op 4 Pers cena 1', '', 'inherit', 'open', 'closed', '', '1614_op-4-pers-cena-1', '', '', '2019-03-16 04:02:34', '2019-03-16 07:02:34', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1614_Op-4-Pers-cena-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(57, 1, '2019-03-16 04:02:37', '2019-03-16 07:02:37', '', '1614_Op 4 Pers cena 2', '', 'inherit', 'open', 'closed', '', '1614_op-4-pers-cena-2', '', '', '2019-03-16 04:02:37', '2019-03-16 07:02:37', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1614_Op-4-Pers-cena-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(58, 1, '2019-03-16 04:02:40', '2019-03-16 07:02:40', '', '1830_Op2 Lilian Img1', '', 'inherit', 'open', 'closed', '', '1830_op2-lilian-img1', '', '', '2019-03-16 04:02:40', '2019-03-16 07:02:40', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1830_Op2-Lilian-Img1.jpg', 0, 'attachment', 'image/jpeg', 0),
(59, 1, '2019-03-16 04:02:42', '2019-03-16 07:02:42', '', '1830_Op2 Lilian Img2', '', 'inherit', 'open', 'closed', '', '1830_op2-lilian-img2', '', '', '2019-03-16 04:02:42', '2019-03-16 07:02:42', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1830_Op2-Lilian-Img2.jpg', 0, 'attachment', 'image/jpeg', 0),
(60, 1, '2019-03-16 04:02:45', '2019-03-16 07:02:45', '', '1830_Op2 Lilian Img3', '', 'inherit', 'open', 'closed', '', '1830_op2-lilian-img3', '', '', '2019-03-16 04:02:45', '2019-03-16 07:02:45', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1830_Op2-Lilian-Img3.jpg', 0, 'attachment', 'image/jpeg', 0),
(61, 1, '2019-03-16 04:02:47', '2019-03-16 07:02:47', '', '1830_Op2 Lilian Img4', '', 'inherit', 'open', 'closed', '', '1830_op2-lilian-img4', '', '', '2019-03-16 04:02:47', '2019-03-16 07:02:47', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1830_Op2-Lilian-Img4.jpg', 0, 'attachment', 'image/jpeg', 0),
(62, 1, '2019-03-16 04:02:50', '2019-03-16 07:02:50', '', '1830_Op2 Lilian Img5', '', 'inherit', 'open', 'closed', '', '1830_op2-lilian-img5', '', '', '2019-03-16 04:02:50', '2019-03-16 07:02:50', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1830_Op2-Lilian-Img5.jpg', 0, 'attachment', 'image/jpeg', 0),
(63, 1, '2019-03-16 04:02:52', '2019-03-16 07:02:52', '', '1830_Op2 Lilian Img6', '', 'inherit', 'open', 'closed', '', '1830_op2-lilian-img6', '', '', '2019-03-16 04:02:52', '2019-03-16 07:02:52', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1830_Op2-Lilian-Img6.jpg', 0, 'attachment', 'image/jpeg', 0),
(64, 1, '2019-03-16 04:02:55', '2019-03-16 07:02:55', '', '1837_Churrasqueira 01', '', 'inherit', 'open', 'closed', '', '1837_churrasqueira-01', '', '', '2019-03-16 04:02:55', '2019-03-16 07:02:55', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1837_Churrasqueira-01.jpg', 0, 'attachment', 'image/jpeg', 0),
(65, 1, '2019-03-16 04:02:57', '2019-03-16 07:02:57', '', '1837_Churrasqueira 012', '', 'inherit', 'open', 'closed', '', '1837_churrasqueira-012', '', '', '2019-03-16 04:02:57', '2019-03-16 07:02:57', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1837_Churrasqueira-012.jpg', 0, 'attachment', 'image/jpeg', 0),
(66, 1, '2019-03-16 04:03:00', '2019-03-16 07:03:00', '', '1837_Churrasqueira', '', 'inherit', 'open', 'closed', '', '1837_churrasqueira', '', '', '2019-03-16 04:03:00', '2019-03-16 07:03:00', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1837_Churrasqueira.jpg', 0, 'attachment', 'image/jpeg', 0),
(67, 1, '2019-03-16 04:03:03', '2019-03-16 07:03:03', '', '1837_Churrasqueira7', '', 'inherit', 'open', 'closed', '', '1837_churrasqueira7', '', '', '2019-03-16 04:03:03', '2019-03-16 07:03:03', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1837_Churrasqueira7.jpg', 0, 'attachment', 'image/jpeg', 0),
(68, 1, '2019-03-16 04:03:05', '2019-03-16 07:03:05', '', '1837_I.s. Hóspedes 01', '', 'inherit', 'open', 'closed', '', '1837_i-s-hospedes-01', '', '', '2019-03-16 04:03:05', '2019-03-16 07:03:05', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1837_I.s.-Hóspedes-01.jpg', 0, 'attachment', 'image/jpeg', 0),
(69, 1, '2019-03-16 04:03:08', '2019-03-16 07:03:08', '', '1837_I.s. Hóspedes 02', '', 'inherit', 'open', 'closed', '', '1837_i-s-hospedes-02', '', '', '2019-03-16 04:03:08', '2019-03-16 07:03:08', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1837_I.s.-Hóspedes-02.jpg', 0, 'attachment', 'image/jpeg', 0),
(70, 1, '2019-03-16 04:03:10', '2019-03-16 07:03:10', '', '1837_I.s. Hóspedes 03', '', 'inherit', 'open', 'closed', '', '1837_i-s-hospedes-03', '', '', '2019-03-16 04:03:10', '2019-03-16 07:03:10', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1837_I.s.-Hóspedes-03.jpg', 0, 'attachment', 'image/jpeg', 0),
(71, 1, '2019-03-16 04:03:14', '2019-03-16 07:03:14', '', '1837_Suíte Casal Op.2 - 01', '', 'inherit', 'open', 'closed', '', '1837_suite-casal-op-2-01', '', '', '2019-03-16 04:03:14', '2019-03-16 07:03:14', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1837_Suíte-Casal-Op.2-01.jpg', 0, 'attachment', 'image/jpeg', 0),
(72, 1, '2019-03-16 04:03:17', '2019-03-16 07:03:17', '', '1837_Suíte Casal Op.2 - 02', '', 'inherit', 'open', 'closed', '', '1837_suite-casal-op-2-02', '', '', '2019-03-16 04:03:17', '2019-03-16 07:03:17', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1837_Suíte-Casal-Op.2-02.jpg', 0, 'attachment', 'image/jpeg', 0),
(73, 1, '2019-03-16 04:03:20', '2019-03-16 07:03:20', '', '1837_Suíte Casal Op.2 - 03', '', 'inherit', 'open', 'closed', '', '1837_suite-casal-op-2-03', '', '', '2019-03-16 04:03:20', '2019-03-16 07:03:20', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1837_Suíte-Casal-Op.2-03.jpg', 0, 'attachment', 'image/jpeg', 0),
(74, 1, '2019-03-16 04:03:24', '2019-03-16 07:03:24', '', '1837_Suíte Casal Op.2 - 04', '', 'inherit', 'open', 'closed', '', '1837_suite-casal-op-2-04', '', '', '2019-03-16 04:03:24', '2019-03-16 07:03:24', '', 49, 'http://localhost/projetos/marianagravon_site/wp-content/uploads/2019/03/1837_Suíte-Casal-Op.2-04.jpg', 0, 'attachment', 'image/jpeg', 0),
(75, 1, '2019-03-16 04:10:13', '2019-03-16 07:10:13', '', 'Destaque 2', '', 'publish', 'closed', 'closed', '', 'destaque-2', '', '', '2019-03-16 05:25:42', '2019-03-16 08:25:42', '', 0, 'http://localhost/projetos/marianagravon_site/?post_type=destaque&#038;p=75', 0, 'destaque', '', 0),
(76, 1, '2019-03-16 04:10:40', '2019-03-16 07:10:40', '', 'Destaque 3', '', 'publish', 'closed', 'closed', '', 'destaque-3', '', '', '2019-03-16 05:25:38', '2019-03-16 08:25:38', '', 0, 'http://localhost/projetos/marianagravon_site/?post_type=destaque&#038;p=76', 0, 'destaque', '', 0),
(77, 1, '2019-03-16 04:11:10', '2019-03-16 07:11:10', '', 'Destaque 4', '', 'publish', 'closed', 'closed', '', 'destaque-4', '', '', '2019-03-16 05:24:15', '2019-03-16 08:24:15', '', 0, 'http://localhost/projetos/marianagravon_site/?post_type=destaque&#038;p=77', 0, 'destaque', '', 0),
(78, 1, '2019-03-16 05:41:46', '2019-03-16 08:41:46', ' ', '', '', 'publish', 'closed', 'closed', '', '78', '', '', '2019-03-16 16:49:52', '2019-03-16 19:49:52', '', 0, 'http://localhost/projetos/marianagravon_site/?p=78', 5, 'nav_menu_item', '', 0),
(79, 1, '2019-03-16 05:41:46', '2019-03-16 08:41:46', ' ', '', '', 'publish', 'closed', 'closed', '', '79', '', '', '2019-03-16 16:49:52', '2019-03-16 19:49:52', '', 0, 'http://localhost/projetos/marianagravon_site/?p=79', 2, 'nav_menu_item', '', 0),
(80, 1, '2019-03-16 05:41:46', '2019-03-16 08:41:46', '', 'Inicial', '', 'publish', 'closed', 'closed', '', '80', '', '', '2019-03-16 16:49:52', '2019-03-16 19:49:52', '', 0, 'http://localhost/projetos/marianagravon_site/?p=80', 1, 'nav_menu_item', '', 0),
(81, 1, '2019-03-16 06:09:23', '2019-03-16 09:09:23', '<label for=\"nome\">Seu nome</label>[text* nome id:nome class:nome placeholder \"Seu nome\"]<label for=\"email\">E-mail</label>[email* email id:email class:email placeholder \"E-mail\"]<label for=\"mensagem\">Mensagem</label>[textarea projeto id:projeto class:projeto placeholder \"Qual é seu projeto?\"]<div class=\"btnSubmitFormContato\">[submit id:enviar class:enviar \"Entrar em contato\"]</div>\n1\nMariana Gravon Arquitetura \"[your-subject]\"\nMariana Gravon Arquitetura <devhcdesenvolvimentos@gmail.com>\ndevhcdesenvolvimentos@gmail.com\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Mariana Gravon Arquitetura (http://localhost/projetos/marianagravon_site)\nReply-To: [your-email]\n\n\n\n\nMariana Gravon Arquitetura \"[your-subject]\"\nMariana Gravon Arquitetura <devhcdesenvolvimentos@gmail.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Mariana Gravon Arquitetura (http://localhost/projetos/marianagravon_site)\nReply-To: devhcdesenvolvimentos@gmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Formulário de Contato', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2019-03-16 06:14:25', '2019-03-16 09:14:25', '', 0, 'http://localhost/projetos/marianagravon_site/?post_type=wpcf7_contact_form&#038;p=81', 0, 'wpcf7_contact_form', '', 0),
(82, 1, '2019-03-16 16:49:37', '2019-03-16 19:49:37', '', 'Portfólio', '', 'publish', 'closed', 'closed', '', 'portfolio', '', '', '2019-03-16 16:49:52', '2019-03-16 19:49:52', '', 0, 'http://localhost/projetos/marianagravon_site/?p=82', 4, 'nav_menu_item', '', 0),
(83, 1, '2019-03-16 16:49:37', '2019-03-16 19:49:37', '', 'Serviços', '', 'publish', 'closed', 'closed', '', 'servicos', '', '', '2019-03-16 16:49:52', '2019-03-16 19:49:52', '', 0, 'http://localhost/projetos/marianagravon_site/?p=83', 3, 'nav_menu_item', '', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `mg_termmeta`
--

DROP TABLE IF EXISTS `mg_termmeta`;
CREATE TABLE IF NOT EXISTS `mg_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `mg_terms`
--

DROP TABLE IF EXISTS `mg_terms`;
CREATE TABLE IF NOT EXISTS `mg_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `mg_terms`
--

INSERT INTO `mg_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0),
(2, 'Residencial', 'residencial', 0),
(3, 'Comercial', 'comercial', 0),
(4, 'Edifício', 'edificio', 0),
(5, 'Menu Principal', 'menu-principal', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `mg_term_relationships`
--

DROP TABLE IF EXISTS `mg_term_relationships`;
CREATE TABLE IF NOT EXISTS `mg_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `mg_term_relationships`
--

INSERT INTO `mg_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(48, 4, 0),
(47, 2, 0),
(46, 3, 0),
(78, 5, 0),
(79, 5, 0),
(80, 5, 0),
(83, 5, 0),
(82, 5, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `mg_term_taxonomy`
--

DROP TABLE IF EXISTS `mg_term_taxonomy`;
CREATE TABLE IF NOT EXISTS `mg_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `mg_term_taxonomy`
--

INSERT INTO `mg_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'categoriaPortfolio', '', 0, 1),
(3, 3, 'categoriaPortfolio', '', 0, 1),
(4, 4, 'categoriaPortfolio', '', 0, 1),
(5, 5, 'nav_menu', '', 0, 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `mg_usermeta`
--

DROP TABLE IF EXISTS `mg_usermeta`;
CREATE TABLE IF NOT EXISTS `mg_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `mg_usermeta`
--

INSERT INTO `mg_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'marianagravon'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'mg_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'mg_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(15, 1, 'show_welcome_panel', '1'),
(24, 1, 'session_tokens', 'a:3:{s:64:\"1c82957d809ddd7f4c2756bfd9f92defe0e263a802c3ccbe50ea79d444d47159\";a:4:{s:10:\"expiration\";i:1552823919;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36\";s:5:\"login\";i:1552651119;}s:64:\"4b188ac303cfe7cf1766ba64a2af2ac8d69ec3592ed23238776f52f5749e621c\";a:4:{s:10:\"expiration\";i:1552892008;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:78:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0\";s:5:\"login\";i:1552719208;}s:64:\"7ac7f56d99fc4d8b20c01adbbe4f86054bcd6597b5db0cc3944fef83c8b85949\";a:4:{s:10:\"expiration\";i:1552934508;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36\";s:5:\"login\";i:1552761708;}}'),
(17, 1, 'mg_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'closedpostboxes_page', 'a:2:{i:0;s:9:\"submitdiv\";i:1;s:13:\"pageparentdiv\";}'),
(19, 1, 'metaboxhidden_page', 'a:5:{i:0;s:10:\"postcustom\";i:1;s:16:\"commentstatusdiv\";i:2;s:11:\"commentsdiv\";i:3;s:7:\"slugdiv\";i:4;s:9:\"authordiv\";}'),
(20, 1, 'meta-box-order_page', 'a:3:{s:4:\"side\";s:36:\"submitdiv,pageparentdiv,postimagediv\";s:6:\"normal\";s:57:\"postcustom,commentstatusdiv,commentsdiv,slugdiv,authordiv\";s:8:\"advanced\";s:0:\"\";}'),
(21, 1, 'screen_layout_page', '2'),
(22, 1, 'mg_user-settings', 'editor_expand=off&libraryContent=browse&editor=tinymce'),
(23, 1, 'mg_user-settings-time', '1552662944'),
(25, 1, 'mg_r_tru_u_x', 'a:2:{s:2:\"id\";i:0;s:7:\"expires\";i:1552740542;}'),
(26, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(27, 1, 'metaboxhidden_nav-menus', 'a:5:{i:0;s:23:\"add-post-type-portfolio\";i:1;s:21:\"add-post-type-servico\";i:2;s:22:\"add-post-type-destaque\";i:3;s:12:\"add-post_tag\";i:4;s:22:\"add-categoriaPortfolio\";}'),
(28, 1, 'nav_menu_recently_edited', '5');

-- --------------------------------------------------------

--
-- Estrutura da tabela `mg_users`
--

DROP TABLE IF EXISTS `mg_users`;
CREATE TABLE IF NOT EXISTS `mg_users` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `mg_users`
--

INSERT INTO `mg_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'marianagravon', '$P$BDU/iJfr0h9xuoaJkN45MLQ.G0UJrp1', 'marianagravon', 'devhcdesenvolvimentos@gmail.com', '', '2019-03-14 19:32:23', '', 0, 'marianagravon');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
