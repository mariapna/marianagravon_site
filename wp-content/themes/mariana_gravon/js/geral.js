$(function(){


	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	//CARROSSEL DE DESTAQUE
	$("#carrosselDestaque").owlCarousel({
		items : 1,
        dots: true,
        loop: true,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	   
        autoplay:true,    
	    autoplayTimeout:3000,
	    autoplayHoverPause:false,
	    scroll:true,
	    navigation: true,
	    smartSpeed: 450,	   		    
	    
	});
	//BOTÕES DO CARROSSEL
	var carrosselDestaque = $("#carrosselDestaque").data('owlCarousel');
	$('#flechaEsquerda').click(function(){ carrosselDestaque.prev(); });
	$('#flechaDireita').click(function(){ carrosselDestaque.next(); });

	$('.pg-portfolio .portfolio .btnAbrirMenuCategorias').click(function(){
		$('.pg-portfolio .portfolio .indice-mobile').addClass('abrir');
		$('body').addClass('stopScroll');
		$('.pg-portfolio .portfolio .lente').css('display','inline-block');
	});

	$('.pg-portfolio .portfolio .indice-mobile span').click(function(){
		$('.pg-portfolio .portfolio .indice-mobile').removeClass('abrir');
		$('body').removeClass('stopScroll');
		$('.pg-portfolio .portfolio .lente').css('display','none');
	});

	$('.pg-portfolio .portfolio .lente').click(function(){
		$('.pg-portfolio .portfolio .indice-mobile').removeClass('abrir');
		$('body').removeClass('stopScroll');
		$('.pg-portfolio .portfolio .lente').css('display','none');
	});

	$('.pg-portfolio .portfolio .lista-projetos ul li .txt-btn .btnVerMais a').click(function(){
		event.preventDefault();
	});

	

	var userFeed = new Instafeed({
       get: 'user',
       userId: '11635157636',
       clientId: '3fbc1cde73aa4fa49f5c4e9e74f97803',
       accessToken: '11635157636.1677ed0.702d4869149b423f889d49359ebe4e6a',
       resolution: 'standard_resolution',
       template: '<a href="{{link}}" target="_blank" id="{{id}}"><div class="itemInstagram" style="background:url({{image}})"><small class="likeComments"><span class="likes">{{likes}}</span><span class="comments">{{comments}}</span></small></div></a>',
       sortBy: 'most-recent',
       limit: 6,
       links: false
     });
     userFeed.run();

     /**********************************************************************
		SCRIPTS FANCYBOX PARA A GALERIA DE IMAGENS 
	**********************************************************************/
	   	$("a#fancy").fancybox({
			'titleShow' : false,
			openEffect	: 'elastic',
			closeEffect	: 'elastic',
			closeBtn    : true,
			arrows      : true,
			nextClick   : true
		});


		//CLIQUE FECHAR MODAL
	$('.pg-projeto .secao-projeto .voltar span').click(function(){
		$('body').removeClass('stopScroll');
		$('.pg-projeto').removeClass('openPopUpPortfolio');
		$('#areaCarrossePortfolio').children().remove();
		$('#carrosselPortfolio').remove();
	});

	//CLIQUE ABRIR MODAL
 	$('.pg-portfolio .portfolio .lista-projetos ul li').click(function(e){
 		//AÇÕES DA MODAL
 		$('.pg-projeto').addClass('openPopUpPortfolio');
 		$('body').addClass('stopScroll');

 		//RECUPERANDO DADOS
 		var galeria = $(this).attr("dataGaleria");
 		var dataFotoDestacada = $(this).attr("dataFotoDestacada");
 		var dataTitle = $(this).attr("dataTitle");
 		var dataDesc = $(this).attr("dataDesc");
 		var galeria = galeria.split(" | ");
 		
 		//PASSANDO DADOS
 		$('.pg-projeto .secao-projeto .fotos-projeto .principal img.fotoDestacada').attr('src',dataFotoDestacada);
 		$('.pg-projeto .secao-projeto .fotos-projeto .principal img.fotoDestacada').attr('alt',dataTitle);
 		$('.pg-projeto .secao-projeto .fotos-projeto .principal a').attr('href',dataFotoDestacada);
 		$('.pg-projeto .secao-projeto h3').text(dataDesc);

 		//ADICIONANDO CARROSSEL NA PÁGINA
		$('#areaCarrossePortfolio').append("<div id='carrosselPortfolio' class='owl-Carousel'></div>");

		// PLAY NO FOR PARA PERCORRER GALERIA RECUPERADA/ SETTIMEOUT PARA EXECUTAR DEPOIS DE  AICIONAR CARROSSEL
		setTimeout(function(){ 
			//ADICIONANDO ITENS
			for (var i = 0 ; i < galeria.length - 1; i++) {
				$('#carrosselPortfolio').append("	<div class='item menor' dataUrl='"+galeria[i]+"' style='background-image: url("+galeria[i]+")'><img src='"+galeria[i]+"' alt='"+dataTitle+"'></div>");	
			}
		}, 10);

		//ACIPNANDO SCRIPT CARROSSEL
		setTimeout(function(){ 
			$("#carrosselPortfolio").owlCarousel({
				items : 4,
				loop: true,
				lazyLoad: true,
				mouseDrag:true,
				touchDrag  : true,	   
				autoplay:true,    
				autoplayTimeout:3000,
				autoplayHoverPause:false,
				scroll:true,
				navigation: true,
				smartSpeed: 450,

				    //CARROSSEL RESPONSIVO
				    responsiveClass:true,			    
			        responsive:{
			            320:{
			                items:1
			            },
			            600:{
			                items:2
			            },

			            991:{
			                items:2
			            },
			            1024:{
			                items:4
			            },
			            1440:{
			                items:4
			            },

			        }

				});
			//BOTÕES DO CARROSSEL DE PROJETO
			var carrosselPortfolio = $("#carrosselPortfolio").data('owlCarousel');
			$('#flechaPrev').click(function(){ carrosselPortfolio.prev(); });
			$('#flechaNext').click(function(){ carrosselPortfolio.next(); });

				$('.pg-projeto .secao-projeto .fotos-projeto .carrossel .menor').click(function(e){
					var foto = $(this).attr('dataUrl');
					$('.pg-projeto .secao-projeto .fotos-projeto .principal img.fotoDestacada').attr('src',foto);
					$('.pg-projeto .secao-projeto .fotos-projeto .principal a').attr('href',foto);
				});
		}, 100);

		
	});
		
});