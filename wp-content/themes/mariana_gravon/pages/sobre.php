<?php
/**
 * Template Name: Sobre
 * Description: Página de sobre
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package marianagravon
 */
get_header();
?>

<!-- PÁGINA SOBRE -->
<div class="pg pg-sobre">
	<div class="sobre">
		<div class="row">
			<div class="texto-imagem">
				<div class="col-sm-5 col-desktop">
					<figure>
						<img src="<?php echo $configuracao['opt_foto']['url'] ?>" alt="<?php echo $configuracao['opt_titulo'] ?>">
					</figure>
				</div>
				<div class="col-sm-7">
					<div class="texto">
						<h4><?php echo $configuracao['opt_titulo'] ?></h4>
						<?php echo $configuracao['opt_desc'] ?>
						<div class="btnEntreEmContato"><a href="<?php echo home_url('/contato/');?>" target="_blank">Entre em contato</a></div>
					</div>
				</div>
				<div class="col-sm-5 col-mobile">
					<figure>
						<img src="<?php echo $configuracao['opt_foto']['url'] ?>" alt="<?php echo $configuracao['opt_titulo'] ?>">
					</figure>
				</div>
			</div>
		</div>

		<div class="conhecao-portfolio">
			<a href="<?php echo home_url('/portfolio/');?>">Conheça o portfólio</a>
		</div>

		<div class="containerFull">
			<div class="instagram">
				<div class="titulo-imagem">
					<img src="<?php echo get_template_directory_uri() ?>/img/photo.png" alt="Instagram">
					<h3>@marianagavron no Instagram</h3>
				</div>
				<div id="instafeed"></div>
			</div>
		</div>
	</div>
</div>

<?php get_footer();