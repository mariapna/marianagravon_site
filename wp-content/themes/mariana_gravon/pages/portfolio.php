<?php
/**
* Template Name: Portfolio
* Description: Página de portfolio
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package marianagravon
*/
get_header();
?>

<!-- PÁGINA DE PORTFÓLIO -->
<div class="pg pg-portfolio">
	<div class="destaque">
		<h1>portfólio</h1>
	</div>
	<div class="containerFull">
		<div class="portfolio">
			<span class="btnAbrirMenuCategorias">categorias</span>
			<ul class="indice indice-desktop">
				<li>todos</li>
				<li>residenciais</li>
				<li>comerciais</li>
				<li>edifícios</li>
			</ul>
			<ul class="indice indice-mobile">
				<span>X</span>
				<li>todos</li>
				<li>residenciais</li>
				<li>comerciais</li>
				<li>edifícios</li>
			</ul>
			<div class="lente"></div>
			<section class="lista-projetos">
				<h6 class="hidden">SEÇÃO PROJETOS</h6>
				<ul>
					<?php

					$portfolio = new WP_Query( array('post_type' => 'portfolio', 'orderby' => 'id', 'posts_per_page' => -1) );

					while($portfolio->have_posts()): $portfolio->the_post();
						$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$foto = $foto[0];

					?>
					<li>
						<img src="<?php echo $foto; ?>" alt="">
						<div class="lente-projeto"></div>
						<div class="txt-btn">
							<p><?php echo get_the_title(); ?></p>
							<div class="btnVerMais"><a href="#">ver mais</a></div>
						</div>
					</li>
					<?php endwhile; wp_reset_query(); ?>

				</ul>
			</section>

			<div class="btnContato"><a href="#">entre em contato</a></div>

			<div class="instagram">
				<div class="titulo-imagem">
					<img src="<?php echo get_template_directory_uri() ?>/img/photo.png" alt="">
					<h3>@marianagavron no Instagram</h3>
				</div>
				<div id="instafeed"></div>
			</div>
		</div>
	</div>
</div>

<!-- PÁGINA DE PROJETO -->
<div class="pg pg-projeto">
	<div class="containerFull">
		<section class="secao-projeto">
			<div class="voltar">
				<span href="#"><i class="fas fa-arrow-left"></i></span>
			</div>

			<h3>Cliente - Data - Tipo</h3>
			
			<div class="fotos-projeto">
				<figure class="principal">
					<img src="<?php echo get_template_directory_uri() ?>/img/banner.jpg" alt="">
				</figure>
				<div class="carrossel">
					<div id="carrosselPortfolio" class="owl-Carousel">
						<div class="item menor" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/banner.jpg')">
							<img src="<?php echo get_template_directory_uri() ?>/img/banner.jpg" alt="">
						</div>
						<div class="item menor" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/banner2.jpeg')">
							<img id="foto1" src="<?php echo get_template_directory_uri() ?>/img/banner2.jpeg" alt="">
						</div> 
						<div class="item menor" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/banner3.jpg')">
							<img src="<?php echo get_template_directory_uri() ?>/img/banner3.jpg" alt="">
						</div>
						<div class="item menor" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/banner4.jpg')">
							<img src="<?php echo get_template_directory_uri() ?>/img/banner4.jpg" alt="">
						</div>
						<div class="item menor" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/banner5.jpg')">
							<img src="<?php echo get_template_directory_uri() ?>/img/banner5.jpg" alt="">
						</div>
					</div>

					<!-- BOTÕES CARROSSEL -->
					<div class="btnCarrosselProjeto">
						<span id="flechaPrev" class="flecha-prev"><img src="<?php echo get_template_directory_uri() ?>/img/prev-page.png" alt=""></span>
						<span id="flechaNext" class="flecha-next"><img src="<?php echo get_template_directory_uri() ?>/img/next-page.png" alt=""></span>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<?php get_footer();