<?php
/**
 * Template Name: Inicial
 * Description: Página Inicial
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package marianagravon
 */
get_header();
?>

<!-- PG INICIAL -->
<div class="pg pg-inicial">
	<div class="destaque-inicial">
		<div class="row">
			<div class="col-sm-4">
				<div class="texto">
					<p><?php echo $configuracao['opt_texto_destaque'] ?></p>
				</div>
			</div>
			<div class="col-sm-8">
				<!-- CARROSSEL DE DESTAQUE -->
				<section class="carrosselDestaque sessao">
					<div id="carrosselDestaque" class="owl-Carousel">
						<?php 
							//LOOP DE POST DESTAQUES
							$destaque = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => 4) );
							while ( $destaque->have_posts() ) : $destaque->the_post();
								// FOTO DESTACADA
								$fotoDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								$fotoDestaque = $fotoDestaque[0];
								$linkDestaque = rwmb_meta('MarianaGravon_link_destaque');
								$checkebox_destaque = rwmb_meta('MarianaGravon_checkebox_destaque');
								$sombra = "class='sobraDestaque'";
						?>
						<div class="item" style="background-image: url(<?php echo $fotoDestaque ?>);">
							<a href="<?php echo $linkDestaque ?>" target="_blank" <?php if ($checkebox_destaque != "") {echo $sombra;} ?> >
								<img class="img-responsive" src="<?php echo $fotoDestaque ?>" alt="<?php echo get_the_title() ?> ">
							</a>
						</div>
						<?php endwhile; wp_reset_query(); ?>
					</div>

					<!-- BOTÕES CARROSSEL SIDEBAR -->
					<div class="btnCarrossel">
						<button id="flechaEsquerda" class="flecha-esquerda"><img src="<?php echo get_template_directory_uri() ?>/img/flecha-esquerda.png" alt="Flexas"></button>
						<button id="flechaDireita" class="flecha-direita"><img src="<?php echo get_template_directory_uri() ?>/img/flecha-direita.png" alt="Flexas"></button>
					</div>

				</section>
			</div>
		</div>
	</div>

	<div class="containerFull">
		<div class="projetos">
			<ul>
				<?php 
					//LOOP DE POST DESTAQUES
					$portfolio = new WP_Query( array( 'post_type' => 'portfolio', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
					while ( $portfolio->have_posts() ) : $portfolio->the_post();
						$galeriaPortfolio = rwmb_meta('MarianaGravon_fotos_projeto');
				?>
				<li class="projeto">
					<ul>
						<?php 
							$i = 0;
							foreach ($galeriaPortfolio as $galeriaPortfolio):
								$galeriaPortfolio = $galeriaPortfolio['full_url'];
								if ($i < 4):
						?>
						<li>
							<a href="<?php echo get_permalink(); ?>">
								<img src="<?php echo $galeriaPortfolio  ?>" alt="<?php echo get_the_title(); ?> ">
							</a>
						</li>
						<?php endif;$i++;endforeach; ?>
					</ul>
				</li>
				<?php endwhile; wp_reset_query(); ?>
			</ul>
		</div>

		<section class="secao-servicos">
			<h6 class="hidden">SEÇÃO SERVIÇOS</h6>

			<ul>
				<?php 
					//LOOP DE POST DESTAQUES
					$servicos = new WP_Query( array( 'post_type' => 'servico', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
					while ( $servicos->have_posts() ) : $servicos->the_post();
						// FOTO DESTACADA
						$fotoServico = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoServico = $fotoServico[0];
						$linkServico = rwmb_meta('MarianaGravon_descricao_servico');
				?>
				<li>
					<div class="servico-inicial">
						<img src="<?php echo $fotoServico  ?>" alt="<?php echo get_the_title() ?>">
						<p><?php echo get_the_title() ?></p>
					</div>
					<div class="servico-hover">
						<a href="<?php echo home_url('/servicos/'); ?> ">
							<p class="titulo"><?php echo get_the_title() ?></p>
							<img src="<?php echo $fotoServico  ?>" alt="<?php echo get_the_title() ?>">
							<p class="texto-servico"><?php echo $linkServico  ?></p>
							<div class="btnSaibaMais"><a href="<?php echo home_url('/servicos/'); ?> ">Saiba Mais</a></div>
						</a>
					</div>
				</li>
				<?php endwhile; wp_reset_query(); ?>
			</ul>
		</section>

		<div class="instagram">
			<div class="titulo-imagem">
				<img src="<?php echo $configuracao['opt_icone_intagram']['url'] ?>" alt="<?php echo get_bloginfo() ?>">
				<h3><a href="<?php echo $configuracao['opt_instagram'] ?>" target="_blank"><?php echo $configuracao['opt_titulo_instagram'] ?></a></h3>
			</div>
			<div id="instafeed"></div>
		</div>
	</div>
</div>

<?php get_footer();