<?php
/**
 * Template Name: Serviços
 * Description: Página de serviços
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package marianagravon
 */
get_header();
?>

<!-- PÁGINA DE SERVIÇOS -->
<div class="pg pg-servicos">
	<div class="containerServicos">
		<div class="servicos">
			<div class="row">
				<div class="col-sm-6">

					<?php
					$contador = 0;
					$servico = new WP_query( array('post_type' => 'servico', 'orderby' => 'id', 'posts_per_page' => -1) );
					while($servico->have_posts()): $servico->the_post();
						$imagem = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$imagem = $imagem[0];

						$contador++;
						if($contador % 2 != 0):

					?>
					<div class="servico">
						<div class="img-titulo">
							<img src="<?php echo $imagem ?>" alt="">
							<h2><?php echo get_the_title(); ?></h2>
						</div>
						<div class="texto">
							<?php echo get_the_content(); ?>
						</div>
					</div>

					<?php endif; endwhile; wp_reset_query(); ?>
					
				</div>
				<div class="col-sm-6">

					<?php
					$contador2 = 0;
					$servico = new WP_query( array('post_type' => 'servico', 'orderby' => 'id', 'posts_per_page' => -1) );
					while($servico->have_posts()): $servico->the_post();
						$imagem = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$imagem = $imagem[0];

						$contador2++;
						if($contador2 % 2 == 0):
					?>
					<div class="servico">
						<div class="img-titulo">
							<img src="<?php echo $imagem ?>" alt="">
							<h2><?php echo get_the_title(); ?></h2>
						</div>
						<div class="texto">
							<?php echo get_the_content(); ?>
						</div>
					</div>
					<?php endif; endwhile; wp_reset_query(); ?>
				</div>
			</div>

			<div class="btnContato"><a href="#">Entre em contato</a></div>
		</div>
	</div>
</div>

<?php get_footer();