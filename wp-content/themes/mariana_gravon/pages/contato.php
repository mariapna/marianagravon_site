<?php
/**
* Template Name: Contato
* Description: Página de contato
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package marianagravon
*/
get_header();
?>

<!-- PÁGINA DE CONTATO -->
<div class="pg pg-contato">
	<div class="containerFull">
		<div class="contato">
			<div class="row">
				<div class="col-md-6">
					<div class="form">
						<?php echo do_shortcode('[contact-form-7 id="81" title="Formulário de Contato"]'); ?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="info-contato">
						<h4><?php echo $configuracao['opt_nome'] ?></h4>
						<div class="endereco">
							<?php echo $configuracao['opt_endereco'] ?>
						</div>
						<div class="email-fone">
							<p class="email"><?php echo $configuracao['opt_email'] ?></p>
							<p class="telefone"><?php echo $configuracao['opt_phone'] ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="instagram">
			<div class="titulo-imagem">
				<img src="<?php echo $configuracao['opt_icone_intagram']['url'] ?>" alt="<?php echo get_bloginfo() ?>">
				<h3><a href="<?php echo $configuracao['opt_instagram'] ?>" target="_blank"><?php echo $configuracao['opt_titulo_instagram'] ?></a></h3>
			</div>
			<div id="instafeed"></div>
		</div>
	</div>
</div>

<?php get_footer();