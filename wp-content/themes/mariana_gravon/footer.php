<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package marianagravon
 */
global $configuracao;
?>

		<!-- RODAPÉ -->
		<footer>
			<div class="containerFull">
				<div class="rodape"></div>
				<div class="copyright">
					<ul class="rede-social-footer">
							<?php if ($configuracao['opt_facebook']): ?>
							<li>
								<a href="<?php echo $configuracao['opt_facebook'] ?>"><i class="fab fa-facebook-f"></i></a>
							</li>
							<?php endif; if ($configuracao['opt_instagram']): ?>
							<li>
								<a href="<?php echo $configuracao['opt_instagram'] ?>"><i class="fab fa-instagram"></i></a>
							</li>
							<?php endif; if ($configuracao['opt_pinterest']): ?>
							<li>
								<a href="<?php echo $configuracao['opt_pinterest'] ?>"><i class="fab fa-pinterest"></i></a>
							</li>
							<?php endif; if ($configuracao['opt_linkedin']): ?>
							<li>
								<a href="<?php echo $configuracao['opt_linkedin'] ?>"><i class="fab fa-linkedin-in"></i></a>
								</li>
								<?php endif; ?>
					</ul>
					<p><i class="fa fa-copyright" aria-hidden="true"></i> <?php echo $configuracao['opt_copyright'] ?></p>
				</div>
			</div>
		</footer>
		<script src="https://matthewelsom.com/assets/js/libs/instafeed.min.js"></script>
	<?php wp_footer(); ?>
	</body>
</html>

