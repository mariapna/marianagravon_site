<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package marianagravon
 */

get_header();

	// RECUPERANDO CATEGORIAS PAI
	$categoriaPortfolio = array(
		'taxonomy'     => 'categoriaPortfolio',
		'child_of'     => 0,
		'parent'       => 0,
		'orderby'      => 'name',
		'pad_counts'   => 0,
		'hierarchical' => 1,
		'title_li'     => '',
		'hide_empty'   => 0
	);
	$listaCategorias = get_categories($categoriaPortfolio);
	$listaCategoriasMobile = get_categories($categoriaPortfolio);
?>

<!-- PÁGINA DE PORTFÓLIO -->
<div class="pg pg-portfolio">
	<div class="destaque">
		<h1 class="hidden">portfólio</h1>
	</div>
	<div class="containerFull">
		<div class="portfolio">
			<span class="btnAbrirMenuCategorias">categorias</span>
			<ul class="indice indice-desktop">
				<li><a href="<?php echo home_url('/portfolio/');?>">todos</a></li>
				<?php foreach ($listaCategorias as $listaCategorias):
					$listaCategorias = $listaCategorias;
				?>
					<li><a href="<?php echo get_category_link($listaCategorias->cat_ID); ?> "><?php echo $listaCategorias->name; ?></a></li>
				<?php endforeach ?>
			</ul>
			<ul class="indice indice-mobile">
				<span>X</span>
				<li><a href="<?php echo home_url('/portfolio/');?>">todos</a></li>
				<?php foreach ($listaCategoriasMobile as $listaCategoriasMobile):
					$listaCategoriasMobile = $listaCategoriasMobile;
				?>
					<li><a href="<?php echo get_category_link($listaCategoriasMobile->cat_ID); ?> "><?php echo $listaCategoriasMobile->name; ?></a></li>
				<?php endforeach ?>
			</ul>
			<div class="lente"></div>
			<section class="lista-projetos">
				<h6 class="hidden">SEÇÃO PROJETOS</h6>
				<?php if ( have_posts() ) : ?>
				<ul>
					<?php

					$j = 0;

					while ( have_posts() ) :the_post();
						$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$foto = $foto[0];
						$galeria_imagens_portfolio = rwmb_meta('MarianaGravon_fotos_projeto');
						$desc_projeto = rwmb_meta('MarianaGravon_desc_projeto');
					?>
					<li dataDesc="<?php echo $desc_projeto ?>" dataTitle="<?php echo get_the_title(); ?>" dataFotoDestacada=" <?php echo $foto ?>" dataGaleria="<?php   
						foreach ($galeria_imagens_portfolio as $galeria_imagens_portfolio) {
							$galeria_imagens_portfolio = $galeria_imagens_portfolio['full_url']." | ";
							echo $galeria_imagens_portfolio;
						}
					?>">
						<img src="<?php echo $foto; ?>" alt="<?php echo get_the_title(); ?>">
						<div class="lente-projeto"></div>
						<div class="txt-btn">
							<p><?php echo get_the_title(); ?></p>
							<div class="btnVerMais"><a href="#">ver mais</a></div>
						</div>
					</li>
					<?php  $j++; endwhile; wp_reset_query(); ?>
				</ul>
				<?php 	endif; ?>
			</section>

			<div class="btnContato"><a href="<?php echo home_url('/contato/'); ?>">entre em contato</a></div>

			<div class="instagram">
				<div class="titulo-imagem">
					<img src="<?php echo get_template_directory_uri() ?>/img/photo.png" alt="">
					<h3>@marianagavron no Instagram</h3>
				</div>
				<div id="instafeed"></div>
			</div>
		</div>
	</div>
</div>

<!-- PÁGINA DE PROJETO -->
<div class="pg pg-projeto">
	<div class="containerFull">
		<section class="secao-projeto">
			<div class="voltar">
				<span href="#"><i class="fas fa-arrow-left"></i></span>
			</div>

			<h3 id="nomeCiente">Cliente - Data - Tipo</h3>
			
			<div class="fotos-projeto">
				<!-- BOTÕES CARROSSEL -->
				<div class="btnCarrosselProjeto">
					<span id="flechaPrev" class="flecha-prev"><img src="<?php echo get_template_directory_uri() ?>/img/flecha-esquerda.png" alt="Flexas"></span>
					<span id="flechaNext" class="flecha-next"><img src="<?php echo get_template_directory_uri() ?>/img/flecha-direita.png" alt="Flexas"></span>
				</div>
				<figure class="principal">
					
					<a href="#" id="fancy" rel="gallery1" >
						<img src=" " alt="" class="fotoDestacada">
					</a>
				</figure>
				<div class="carrossel" id="areaCarrossePortfolio">
				

					
				</div>
			</div>
		</section>
	</div>
</div>


<?php
get_sidebar();
get_footer();
