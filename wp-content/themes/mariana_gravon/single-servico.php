<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package marianagravon
 */

get_header();

?>
<!-- PÁGINA DE SERVIÇOS -->
<div class="pg pg-servicos">
	<div class="containerServicos">
		<div class="servicos">
			<div class="row">
				
				<div class="col-sm-12">

					<?php
					
					/* Start the Loop */
					while ( have_posts() ) : the_post();
						$imagem = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$imagem = $imagem[0];

						
					?>
					<div class="servico">
						<div class="img-titulo">
							<img src="<?php echo $imagem ?>" alt="">
							<h2><?php echo get_the_title(); ?></h2>
						</div>
						<div class="texto">
							<?php echo get_the_content(); ?>
						</div>
					</div>
					<?php  endwhile; wp_reset_query(); ?>
				</div>
			</div>

			<div class="btnContato"><a href="<?php echo home_url('/contato/');?>">Entre em contato</a></div>
		</div>
	</div>
</div>

<?php
get_footer();
