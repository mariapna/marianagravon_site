<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package marianagravon
 */

get_header();
	// RECUPERANDO CATEGORIAS PAI
	$categoriaPortfolio = array(
		'taxonomy'     => 'categoriaPortfolio',
		'child_of'     => 0,
		'parent'       => 0,
		'orderby'      => 'name',
		'pad_counts'   => 0,
		'hierarchical' => 1,
		'title_li'     => '',
		'hide_empty'   => 0
	);
	$listaCategorias = get_categories($categoriaPortfolio);
?>

<!-- PÁGINA DE PORTFÓLIO -->
<div class="pg pg-portfolio">
	<div class="destaque">
		<h1>portfólio</h1>
	</div>
	<div class="containerFull">
		<div class="portfolio">
			<span class="btnAbrirMenuCategorias">categorias</span>
			<ul class="indice indice-desktop">
				<li><a href="<?php echo home_url('/portfolio/');?>">todos</a></li>
				<?php foreach ($listaCategorias as $listaCategorias):
					$listaCategorias = $listaCategorias;
				?>
					<li><a href="<?php echo get_category_link($listaCategorias->cat_ID); ?> "><?php echo $listaCategorias->name; ?></a></li>
				<?php endforeach ?>
			</ul>
			<ul class="indice indice-mobile">
				<span>X</span>
				<li><a href="<?php echo home_url('/portfolio/');?>">todos</a></li>
				<?php foreach ($listaCategorias as $listaCategorias):
					$listaCategorias = $listaCategorias;
				?>
					<li><a href="<?php echo get_category_link($listaCategorias->cat_ID); ?> "><?php echo $listaCategorias->name; ?></a></li>
				<?php endforeach ?>
			</ul>
			<div class="lente"></div>
			<section class="lista-projetos">
				<h6 class="hidden">SEÇÃO PROJETOS</h6>
				<?php if ( have_posts() ) : ?>
				<ul>
					<?php

					$j = 0;

					while ( have_posts() ) :the_post();
						$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$foto = $foto[0];

						$galeria_imagens_portfolio = rwmb_meta('MarianaGravon_fotos_projeto');
					?>
					<li data-ID="<?php echo $j ?>">
						<img src="<?php echo $foto; ?>" alt="">
						<div class="lente-projeto"></div>
						<div class="txt-btn">
							<p><?php echo get_the_title(); ?></p>
							<div class="btnVerMais"><a href="#">ver mais</a></div>
						</div>
					</li>
					<?php  $j++; endwhile; wp_reset_query(); ?>
				</ul>
				<?php 	endif; ?>
			</section>

			<div class="btnContato"><a href="#">entre em contato</a></div>

			<div class="instagram">
				<div class="titulo-imagem">
					<img src="<?php echo get_template_directory_uri() ?>/img/photo.png" alt="">
					<h3>@marianagavron no Instagram</h3>
				</div>
				<div id="instafeed"></div>
			</div>
		</div>
	</div>
</div>

<!-- PÁGINA DE PROJETO -->
<?php
	$i = 0;
	while ( have_posts() ) :the_post();
		$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
		$foto = $foto[0];

		$galeria_imagens_portfolio = rwmb_meta('MarianaGravon_fotos_projeto');
?>
<div class="pg pg-projeto" id="<?php echo $i ?>" >
	<div class="containerFull">
		<section class="secao-projeto">
			<div class="voltar">
				<span href="#"><i class="fas fa-arrow-left"></i></span>
			</div>

			<h3>Cliente - Data - Tipo</h3>
			
			<div class="fotos-projeto">
				<figure class="principal">
					<img src="<?php echo get_template_directory_uri() ?>/img/banner.jpg" alt="">
				</figure>
				<div class="carrossel">
					<div id="carrosselPortfolio<?php echo $i  ?>" class="owl-Carousel">
						<?php foreach ($galeria_imagens_portfolio as $galeria_imagens_portfolio):
							$galeria_imagens_portfolio = $galeria_imagens_portfolio['full_url'];
						?>
						<div class="item menor" style="background-image: url('<?php echo $galeria_imagens_portfolio ?>')">
							<img src="<?php echo $galeria_imagens_portfolio ?>" alt=" <?php echo get_the_title(); ?>">
						</div>
						<?php endforeach; ?>
					</div>

					<!-- BOTÕES CARROSSEL -->
					<div class="btnCarrosselProjeto">
						<span id="flechaPrev" class="flecha-prev"><img src="<?php echo get_template_directory_uri() ?>/img/prev-page.png" alt=""></span>
						<span id="flechaNext" class="flecha-next"><img src="<?php echo get_template_directory_uri() ?>/img/next-page.png" alt=""></span>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<script>
	

	$('.pg-portfolio .portfolio .lista-projetos ul li').click(function(e){
		var dataID = $(this).attr('data-ID');
		$('#'+dataID).css('display', 'block');
		$('body').addClass('stopScroll');
	
		setTimeout(function(){ 
			var owl = $('.owl-carousel');
			owl.removeClass('off');
			owl.addClass('on');
			$("#carrosselPortfolio<?php echo $i  ?>").owlCarousel({
				items : 4,
				dots: true,
				loop: false,
				lazyLoad: true,
				mouseDrag:true,
				touchDrag  : true,	       
				autoplayTimeout:5000,
				autoplayHoverPause:true,
				smartSpeed: 450,

						    //CARROSSEL RESPONSIVO
						    //responsiveClass:true,			    
					 //        responsive:{
					 //            320:{
					 //                items:1
					 //            },
					 //            600:{
					 //                items:2
					 //            },

					 //            991:{
					 //                items:2
					 //            },
					 //            1024:{
					 //                items:3
					 //            },
					 //            1440:{
					 //                items:4
					 //            },

					 //        }

					});
		}, 100);

		
	});
 	
	

	

</script>
<?php  $i++; endwhile; wp_reset_query(); ?>
<?php
get_footer();
