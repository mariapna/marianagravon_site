<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package marianagravon
 */
global $configuracao;

?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<!-- META -->
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" /> 

		<?php wp_head(); ?>
	</head>
	
	<body <?php body_class(); ?>>
		<!-- TOPO -->
		<header class="topo">
			<div class="menu-contato">
				<div class="containerFull">
					<div class="row">
						<div class="col-sm-6">
							<ul class="contato">
								<?php if ($configuracao['opt_phone']): ?>
								<li class="fone"><?php echo $configuracao['opt_phone'] ?></li>
								<?php endif; if ($configuracao['opt_email']): ?>
								<li class="email"><?php echo $configuracao['opt_email'] ?></li>
								<?php endif; ?>
							</ul>
						</div>
						<div class="col-sm-6">
							<ul class="rede-social">
								<?php if ($configuracao['opt_facebook']): ?>
								<li>
									<a href="<?php echo $configuracao['opt_facebook'] ?>"><i class="fab fa-facebook-f"></i></a>
								</li>
								<?php endif; if ($configuracao['opt_instagram']): ?>
								<li>
									<a href="<?php echo $configuracao['opt_instagram'] ?>"><i class="fab fa-instagram"></i></a>
								</li>
								<?php endif; if ($configuracao['opt_pinterest']): ?>
								<li>
									<a href="<?php echo $configuracao['opt_pinterest'] ?>"><i class="fab fa-pinterest"></i></a>
								</li>
								<?php endif; if ($configuracao['opt_linkedin']): ?>
								<li>
									<a href="<?php echo $configuracao['opt_linkedin'] ?>"><i class="fab fa-linkedin-in"></i></a>
								</li>
								<?php endif; ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="menu-principal">
				<div class="containerFull">
					<div class="row">
						<!-- LOGO -->
						<div class="col-sm-3">
							<a href="<?php echo home_url(); ?>">
								<img class="img-responsive" src="<?php echo $configuracao['opt_logo']['url'];  ?>" alt="<?php echo get_bloginfo() ?>">
							</a>
						</div>
						<!-- MENU  -->	
						<div class="col-sm-9">
							<div class="navbar" role="navigation">	
								<!-- MENU MOBILE TRIGGER -->
								<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" data-toggle="collapse" data-target="#collapse">
									<span class="sr-only"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
								<!--  MENU MOBILE-->
								<div class="row navbar-header">			
									<nav class="collapse navbar-collapse" id="collapse">
										<ul class="nav navbar-nav">			
											<?php 
												$menu = array(
													'theme_location'  => '',
													'menu'            => 'Menu Principal',
													'container'       => false,
													'container_class' => '',
													'container_id'    => '',
													'menu_class'      => 'nav navbar-nav',
													'menu_id'         => '',
													'echo'            => true,
													'fallback_cb'     => 'wp_page_menu',
													'before'          => '',
													'after'           => '',
													'link_before'     => '',
													'link_after'      => '',
													'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
													'depth'           => 2,
													'walker'          => ''
													);
												wp_nav_menu( $menu );
											?>
										</ul>
									</nav>						
								</div>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>