<?php

/**
 * Plugin Name: Base Mariana Gravon
 * Description: Controle base do tema Mariana gravon.
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: http://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */


function baseProjeto () {

		// TIPOS DE CONTEÚDO
	conteudosProjeto();

	taxonomiaProjeto();

	metaboxesProjeto();
}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosProjeto (){

		// TIPOS DE CONTEÚDO
		tipoPortfolio();
		tipoServico();
		tipoDestaque();


		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
				$titulo = 'Título do destaque';
				break;
				
				default:
				break;
			}

			return $titulo;

		}

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipoPortfolio() {

		$rotulosPortfolio = array(
			'name'               => 'Portfolio',
			'singular_name'      => 'portfolio',
			'menu_name'          => 'Portfólio',
			'name_admin_bar'     => 'Portfolio',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo portfolio',
			'new_item'           => 'Novo portfolio',
			'edit_item'          => 'Editar portfolio',
			'view_item'          => 'Ver portfolio',
			'all_items'          => 'Todos os portfolio',
			'search_items'       => 'Buscar portfolio',
			'parent_item_colon'  => 'Do portfolio',
			'not_found'          => 'Nenhum portfolio cadastrado.',
			'not_found_in_trash' => 'Nenhum portfolio na lixeira.'
		);

		$argsPortfolio 	= array(
			'labels'             => $rotulosPortfolio,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-desktop',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'portfolio' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','thumbnail')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('portfolio', $argsPortfolio);

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipoServico() {

		$rotulosServico = array(
			'name'               => 'Servicos',
			'singular_name'      => 'servico',
			'menu_name'          => 'Servicos',
			'name_admin_bar'     => 'Serviços',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo servico',
			'new_item'           => 'Novo servico',
			'edit_item'          => 'Editar servico',
			'view_item'          => 'Ver servico',
			'all_items'          => 'Todos os servicos',
			'search_items'       => 'Buscar servico',
			'parent_item_colon'  => 'Dos servicos',
			'not_found'          => 'Nenhum servico cadastrado.',
			'not_found_in_trash' => 'Nenhum servico na lixeira.'
		);

		$argsServico 	= array(
			'labels'             => $rotulosServico,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-editor-ul',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'servicos' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','thumbnail', 'editor')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('servico', $argsServico);

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipoDestaque() {

		$rotulosDestaque = array(
			'name'               => 'Destaques',
			'singular_name'      => 'destaque',
			'menu_name'          => 'Destaques',
			'name_admin_bar'     => 'Destaques',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo destaque',
			'new_item'           => 'Novo destaque',
			'edit_item'          => 'Editar destaque',
			'view_item'          => 'Ver destaque',
			'all_items'          => 'Todos os destaques',
			'search_items'       => 'Buscar destaque',
			'parent_item_colon'  => 'Dos destaques',
			'not_found'          => 'Nenhum destaque cadastrado.',
			'not_found_in_trash' => 'Nenhum destaque na lixeira.'
		);

		$argsDestaque 	= array(
			'labels'             => $rotulosDestaque,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-star-filled',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'destaques' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','thumbnail')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('destaque', $argsDestaque);

	}


	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaProjeto () {		
		taxonomiaCategoriaPorfolio();
	}
		// TAXONOMIA DE DESTAQUE
	function taxonomiaCategoriaPorfolio() {

		$rotulosCategoriaPortfolio = array(
			'name'              => 'Categorias de portfolio',
			'singular_name'     => 'Categoria de portfolio',
			'search_items'      => 'Buscar categorias de portfolio',
			'all_items'         => 'Todas as categorias de portfolio',
			'parent_item'       => 'Categoria de portfolio pai',
			'parent_item_colon' => 'Categoria de portfolio pai:',
			'edit_item'         => 'Editar categoria de portfolio',
			'update_item'       => 'Atualizar categoria de portfolio',
			'add_new_item'      => 'Nova categoria de portfolio',
			'new_item_name'     => 'Nova categoria',
			'menu_name'         => 'Categorias de portfolio',
		);

		$argsCategoriaPortfolio 		= array(
			'hierarchical'      => true,
			'labels'            => $rotulosCategoriaPortfolio,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'categoria-portfolio' ),
		);

		register_taxonomy( 'categoriaPortfolio', array( 'portfolio' ), $argsCategoriaPortfolio );

	}			

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesProjeto(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

	function registraMetaboxes( $metaboxes ){

		$prefix = 'MarianaGravon_';

		// METABOX DE DESTAQUE
		$metaboxes[] = array(

			'id'			=> 'detalhesMetaboxDestaqueInicial',
			'title'			=> 'Detalhes do Destaque',
			'pages' 		=> array( 'destaque' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(
				array(
					'name'  => 'Link Destaque: ',
					'id'    => "{$prefix}link_destaque",
					'desc'  => '',
					'type'  => 'text',
					
				),	
				array(
					'name'  => 'Ativar sombra: ',
					'id'    => "{$prefix}checkebox_destaque",
					'desc'  => '',
					'type'  => 'checkbox',
					
				),	
			),
		);

		// METABOX DO PORTFOLIO
		$metaboxes[] = array(

			'id'			=> 'detalhesMetaboxPortfolio',
			'title'			=> 'Detalhes do Portfolio',
			'pages' 		=> array( 'portfolio' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(

				array(
					'name'  => 'Fotos projeto: ',
					'id'    => "{$prefix}fotos_projeto",
					'desc'  => 'Selecione as fotos para esse projeto. Máximo de 10 fotos.',
					'type'  => 'image_advanced',
					'max_file_uploads' => 10
				),
				array(
					'name'  => 'Detalhes do projeto: ',
					'id'    => "{$prefix}desc_projeto",
					'desc'  => '"Cliente - Data - Tipo"',
					'type'  => 'text',
					'max_file_uploads' => 10
				),
			),
		);

		// METABOX DE DESTAQUE
		$metaboxes[] = array(

			'id'			=> 'detalhesMetaboxServicos',
			'title'			=> 'Detalhes do servico',
			'pages' 		=> array( 'servico' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(

				array(
					'name'  => 'Breve descrição do serviço: ',
					'id'    => "{$prefix}descricao_servico",
					'desc'  => '',
					'type'  => 'textarea',
					
				),	

			),
		);

		return $metaboxes;
	}

	function metaboxjs(){

		global $post;
		$template = get_post_meta($post->ID, '_wp_page_template', true);
		$template = explode('/', $template);
		$template = explode('.', $template[1]);
		$template = $template[0];

		if($template != ''){
			wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
		}
	}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesProjeto(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerProjeto(){

		if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseProjeto');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

		baseProjeto();

		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );